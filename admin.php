<!DOCTYPE html>

	<?php 
		$page = 9; 
		if(!isset($_SESSION)) { 
			session_start(); 
		}
	?>
	
<html lang="en">
	<head>
		<title> จัดการข้อมูล </title>
		<?php include 'config/header.php' ?>
	</head>
	<body>
		<?php include 'navbar.php' ?>
			<div style="padding: 200px; padding-top: 0px">

        <div class="ui segments">
					<div class="ui inverted tertiary segment">

						<div class="ui centered grid">

							<div class="twelve wide column">
								<div class="ui fluid action input">
									<input type="text" placeholder="" id="selectword">
									<select class="ui selection dropdown" id="dd_selectfrom">
										<option value="1">Location Code</option>
										<option value="2">Project id</option>
									</select>
									<div class="ui right labeled icon button" id="btn_select">
										<i class="right arrow icon"></i>เลือก
									</div>
								</div>
							</div>

							<div class="three wide column">
								<div class="ui red button" id="btn_clean">ย้อนกลับทั้งหมด</div>
							</div>
						
						</div> <!-- grid -->

					</div> <!-- segment1 -->

					<div class="ui center aligned secondary segment">
						<div id="project_name"><h3> เลือกโครงการ </h3></div>
        	</div> <!-- segment2 -->

					<div class="ui segment">

							<div class="ui centered four column grid" id="show_info">
								 <div class="column">
										<div class="ui fluid labeled input">
											<div class="ui label"> Project ID </div>
											<input type="text" readonly="" id="project_id">
										</div>
								</div>
								<div class="column">
										<div class="ui fluid labeled input">
											<div class="ui label"> Location Code </div>
											<input type="text" readonly="" id="loc_code">
										</div>
								</div>
								<div class="column">
										<div class="ui fluid labeled input">
											<div class="ui label"> Step </div>
											<input type="text" readonly="" id="flow_step">
										</div>
								</div>
								<div class="column" id="show_btn_toinfo"> </div>
							</div>

					</div> <!-- segment3 -->

					
				</div>
				<div id="show_here">

								<div class="ui styled fluid accordion">
									<div class="title">
										<i class="dropdown icon"></i>
										ไอดี Log
									</div>
									<div class="content">
											<div class="ui centered five column grid">
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> โครงการ </div>
															<input type="text" readonly="" id="p_log">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> มิเตอร์ </div>
															<input type="text" readonly="" id="m_log">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เอกสาร </div>
															<input type="text" readonly="" id="d_log">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> สัญญาเก่า </div>
															<input type="text" readonly="" id="co_log">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> สัญญาใหม่ </div>
															<input type="text" readonly="" id="cn_log">
														</div>
												</div>
												
											</div>
									</div>
									<div class="title">
										<i class="dropdown icon"></i>
										Flow Step
									</div>
									<div class="content">
										
											<div class="ui centered two column grid">
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เริ่มงานเจรจา </div>
															<input type="text" readonly="" id="flow1_accept">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เสร็จงานเจรจา </div>
															<input type="text" readonly="" id="flow1_finish">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เริ่มงานอนุมัติ </div>
															<input type="text" readonly="" id="flow2_accept">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เสร็จงานอนุมัติ </div>
															<input type="text" readonly="" id="flow2_finish">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เริ่มงานรวมเอกสาร </div>
															<input type="text" readonly="" id="flow3_accept">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เสร็จงานรวมเอกสาร </div>
															<input type="text" readonly="" id="flow3_finish">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เริ่มงานตรวจเอกสาร </div>
															<input type="text" readonly="" id="flow4_accept">
														</div>
												</div>
												<div class="column">
														<div class="ui fluid labeled input"> <div class="ui label"> เสร็จงานตรวจเอกสาร </div>
															<input type="text" readonly="" id="flow4_finish">
														</div>
												</div>
											</div>

									</div>
									<div class="title">
										<i class="dropdown icon"></i>
										แก้ไขข้อมูลสัญญา
									</div>
									<div class="content">
										
									</div>
								</div>

				</div>
                
			</div>

	</body>
	
	<?php include 'config/footer.php' ?>
  <script>
	$(document).ready(function () {
			
			$('#dd_selectfrom').dropdown();
			$("#show_here").hide();
			$("#show_info").hide();
			$('.ui.accordion').accordion({
				exclusive: false
			});

			$('#btn_clean').click(function(){
				if(confirm('ย้อนข้อมูล?')){
					$.post('function/admin_cleanall.php', function(){
						location.reload();
					});
				}
			});

			$('#btn_select').click(function() {
					var word = $('#selectword').val();
					var from = $('#dd_selectfrom').val();

					$.post('function/admin_searchproject.php?word='+word+'&from='+from,
						function(out) {

							console.log(out);
							if(out.bool) {

								$('#project_name').html('<h3>โครงการ'+out.data['project_name']+'</h3>');
								var flow_step = '';
								if(out.data['flow_step'] == 0)				flow_step = 'ยังไม่รับงาน';
								else if(out.data['flow_step'] == 1)		flow_step = 'รอเจรจา';
								else if(out.data['flow_step'] == -1)	flow_step = 'เจรจาใหม่';
								else if(out.data['flow_step'] == 2)		flow_step = 'รออนุมัติ';
								else if(out.data['flow_step'] == 3)		flow_step = 'รอทำเอกสาร';
								else if(out.data['flow_step'] == -3)	flow_step = 'ทำเอกสารใหม่';
								else if(out.data['flow_step'] == 4)		flow_step = 'รอตรวจเอกสาร';
								else if(out.data['flow_step'] == 5)		flow_step = 'ส่งสัญญาตั้งเบิก';
								
								$('#project_id').val(out.data['project_id']);
								$('#loc_code').val(out.data['project_location_code']);
								$('#flow_step').val(flow_step);
								$('#show_btn_toinfo').html('<a href="p_information.php?id='+out.data['project_id']+'"class="ui fluid blue button">ไปหน้ารายละเอียด</a>');

								$('#p_log').val(out.data['project_log']);
								$('#m_log').val(out.data['meter_log']);
								$('#d_log').val(out.data['document_log']);
								$('#co_log').val(out.data['contract_log_old']);
								$('#cn_log').val(out.data['contract_log_new']);

								if(out.f.length != 0) {
									$('#flow1_accept').val(out.f['flow1_accept']);
									$('#flow1_finish').val(out.f['flow1_finish']);
									$('#flow2_accept').val(out.f['flow2_accept']);
									$('#flow2_finish').val(out.f['flow2_finish']);
									$('#flow3_accept').val(out.f['flow3_accept']);
									$('#flow3_finish').val(out.f['flow3_finish']);
									$('#flow4_accept').val(out.f['flow4_accept']);
									$('#flow4_finish').val(out.f['flow4_finish']);
								}

								$('#show_info').show(500);
								$("#show_here").show(500);
							}
							else{
								$('#project_name').html('<h3>ไม่เจอข้อมูล</h3>');
							}
							
						},'json'
					);
			});

	});
    
		
  </script>
</html>
