<!-- jquery -->
<script src="assets/js/jquery-3.2.1.min.js"></script>

<!--migrate jquery vertion (for tab history)-->
<script src="assets/js/jquery-migrate-1.4.1.min.js"></script>
<script src="assets/js/jquery-migrate-3.0.0.min.js"></script>
<script src="assets/js/jquery.address-1.5.min.js?strict=false&wrap=true"></script>

<!--moment (time)-->
<script src="assets/js/moment-with-locales.js"></script>

<!-- semantic -->
<script type="text/javascript" src="assets/semantic/dist/semantic.js"></script>
<script src="assets/semantic/dist/components/transition.js"></script>
<script src="assets/semantic/dist/components/popup.js"></script>
<script src="assets/semantic/dist/components/dropdown.js"></script>
<script src="assets/semantic/dist/components/checkbox.js"></script>
<script src="assets/semantic/dist/components/tab.js"></script>
<script src="assets/semantic/dist/components/form.js"></script>
<script src="assets/semantic/dist/components/accordion.js"></script> 

<!--datatable-->
<script type="text/javascript" src="assets/DataTables/DataTables-1.10.15/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/DataTables/AutoFill-2.2.0/js/dataTables.autoFill.min.js"></script>
<script type="text/javascript" src="assets/DataTables/FixedColumns-3.2.2/js/dataTables.fixedColumns.min.js"></script>
<script type="text/javascript" src="assets/DataTables/Responsive-2.1.1/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="assets/DataTables/Scroller-1.4.2/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="assets/DataTables/Select-1.2.2/js/dataTables.select.min.js"></script>