<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- semantic -->
<link rel="stylesheet" type="text/css" href="assets/semantic/dist/semantic.min.css">

<!--datatable-->
<link rel="stylesheet" type="text/css" href="assets/DataTables/DataTables-1.10.15/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="assets/DataTables/AutoFill-2.2.0/css/autoFill.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="assets/DataTables/FixedColumns-3.2.2/css/fixedColumns.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/DataTables/Responsive-2.1.1/css/responsive.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/DataTables/Scroller-1.4.2/css/scroller.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/DataTables/Select-1.2.2/css/select.dataTables.min.css"/>
