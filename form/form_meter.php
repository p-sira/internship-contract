<div class="field">
	<div class="fields">
		<div class="twelve wide field">
			<label>โครงการ</label>
			<input type="text" name="project_name" placeholder="ชื่อโครงการ" readonly="">
		</div>
		<div class="six wide field">
			<label>Location code</label>
			<input type="text" name="project_location_code" placeholder="location code" readonly="">
		</div>
	</div>
	<div class="ui divider"> </div>

	<div class="three fields">
		<div class="field">
				<label>Plan</label>
				<input type="text" name="meter_plan" placeholder="Plan"
				value=""> 
		</div>	
		<div class="field">
				<label>RP</label>
				<input type="text" name="meter_RP" placeholder="RP"
				value=""> 
		</div>
		<div class="field">
				<label>สถานะ</label>
				<select class="ui dropdown" name="meter_status" id="meter_status">
				<option value="">เลือกสถานะ</option>
				<option value="ยกเลิก">ยกเลิก</option>
				<option value="รอเอกสารพื้นที่">รอเอกสารพื้นที่</option>
				<option value="รอเอกสารภายใน">รอเอกสารภายใน</option>
				<option value="ติดปัญหา">ติดปัญหา</option>
				<option value="Finish">Finish</option>
				</select>

		</div>
	</div>

	<div class="fields">
		<div class="eight wide field">
			<label>เลขที่มิเตอร์</label>
			<input type="text" name="meter_number" placeholder="เลขที่มิเตอร์"
			value="">     
		</div>
		<div class="eight wide field">
			<label>เลขที่สัญญา</label>
			<input type="text" name="meter_contract_number" placeholder="เลขที่สัญญา"
			value="">     
		</div>
	</div>

	<div class="two fields">
		<div class="field">
			<label>เอกสารพื้นที่</label>
			<input type="date" name="meter_doc1" value="">
		</div>
		<div class="field">
			<label>เอกสารภายใน</label>
			<input type="date" name="meter_doc2" value="">
		</div>
	</div>

	<div class="three fields">
		<div class="field">
			<label>ยื่นเรื่องการไฟฟ้า</label>
				<input type="date" name="meter_send" placeholder="ยื่นเรื่องการไฟฟ้า"
			value="">
		</div>
		<div class="field">
			<label>ตรวจหน้างาน&ชำระเงิน</label>
			<input type="date" name="meter_check" placeholder="ตรวจหน้างาน&ชำระเงิน"
			value="">
		</div>
		<div class="field">
			<label>Started date</label>
			<input type="date" name="meter_start_date" value="">
		</div>
	</div>
	<div class="two fields">
		<div class="field">
			<label>เลขที่บัญชีแสดงสัญญา/รหัสเครื่องวัด</label>
			<input type="text" name="meter_something_number" placeholder="เลขที่บัญชีแสดงสัญญา/รหัสเครื่องวัด"
			value="">
		</div>
		<div class="field">
			<label>ผู้ทำจ่าย</label>
			<input type="text" name="meter_who_pay" placeholder="ผู้ทำจ่าย"
			value="">
		</div>
	</div>

		<div class="field">
			<label>หมายเหตุอื่นๆ</label>
			<textarea rows="2" name="meter_remark"></textarea>
		</div>
</div>