<div class="field">
	<div class="fields">
		<div class="sixteen wide required field">
			<label>ชื่อโครงการ</label>
			<input type="text" name="project_name" placeholder="ชื่อโครงการ"
				value="">
		</div>
	</div>
	<div class="fields">
		<div class="five wide required field">
			<label>Lot</label>
			<input type="text" name="project_lot" placeholder="lot"
			value="">     
		</div>
		<div class="five wide required field">
			<label>no.</label>
			<input type="text" name="project_number" placeholder="no."
			value="">     
		</div>
				
		<div class="six wide required field">
			<label>Location code</label>
			<input type="text" name="project_location_code" placeholder="location code"
			value="">
		</div>
	</div>

	<div class="two fields">
		<div class="field">
			<label>ชื่อผู้ติดต่อ</label>
			<input type="text" name="lot_person" placeholder="ชื่อผู้ติดต่อ"
			value="">
		</div>
		<div class="field">
			<label>เบอร์โทรศัพท์ผู้ติดต่อ</label>
			<input type="text" name="lot_tel" placeholder="เบอร์โทรศัพท์ผู้ติดต่อ"
			value="">
		</div>
	</div>

	<div class="field">
		<label>ที่อยู่โดยละเอียด</label>
		<textarea rows="2" name="lot_address"></textarea>
	</div>

	<div class="three fields">
		<div class="field">
			<label>เขต</label>
				<input type="text" name="lot_district" placeholder="เขต"
			value="">
		</div>
		<div class="required field">
			<label>สถานะพื้นที่</label>
			<select class="ui dropdown" name="lot_status" id="lot_status">
				<option value="">เลือกสถานะพื้นที่</option>
			</select>
		</div>
		<div class="required field">
			<label>ประเภทพื้นที่</label>
			<select class="ui dropdown" name="lot_type" id="lot_type">
				<option value="">เลือกประเภทพื้นที่</option>
			</select>
		</div>
	</div>
	<div class="three fields">
		<div class="field">
			<label>Node start</label>
			<input type="date" name="date_start" placeholder="Node start date"
			value="">
		</div>
		<div class="field">
			<label>Node finish</label>
			<input type="date" name="date_finish" placeholder="Node finish date"
			value="">
		</div>
		<div class="field">
			<label>Node available</label>
			<input type="date" name="date_on_service" placeholder="Node available date"
			value="">
		</div>
	</div>
</div>