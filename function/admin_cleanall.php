<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $sql = 'UPDATE tb_main
            SET 
                document_log = null,
                contract_log_old = project_id,
                contract_log_new = project_id,
                flow_step = 0,
                flow_log = 0,
                project_log = project_id,
                meter_log = project_id;

            DELETE from tb_document_log;
            DELETE from tb_flow_log;
            DELETE from tb_contract_log 	WHERE log_id > 4477;
            DELETE from tb_project_log 		WHERE log_id > 4477;
            DELETE from tb_meter_log 			WHERE log_id > 4477;
            DELETE from tb_grand_log 			WHERE log_id > 4477;

            ALTER TABLE tb_document_log 	AUTO_INCREMENT = 1;
            ALTER TABLE tb_flow_log 			AUTO_INCREMENT = 1;
            ALTER TABLE tb_contract_log 	AUTO_INCREMENT = 4478;
            ALTER TABLE tb_project_log 		AUTO_INCREMENT = 4478;
            ALTER TABLE tb_meter_log 			AUTO_INCREMENT = 4478;
            ALTER TABLE tb_grand_log 			AUTO_INCREMENT = 4478;';
   
    $mysqli->multi_query($sql);
    $mysqli->close();
    exit;

?>