<?php
    ////////  ไม่ใช้   
    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $results = array();

    $sql = 'SELECT m.project_id, pl.project_location_code, pl.project_name
            FROM tb_main as m
            INNER JOIN tb_project_log as pl ON pl.log_id = m.project_log';
    $result = $mysqli->query($sql);
    if($result->num_rows > 0){
      while($out = $result->fetch_array(MYSQLI_ASSOC)){
        $nested = array(
          'name'  =>  $out['project_name'].' '.$out['project_location_code'],
          'value' =>  $out['project_id']
        );
        $results[] = $nested;
      }

    }

    $data = array(
      'success' => true,
      'results' => $results
    );

    $mysqli->close();
    echo json_encode($data);
    exit();

?>