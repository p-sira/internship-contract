<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $selectword   = $_GET['word'];
    $selectfrom   = $_GET['from'];

    if($selectfrom == 1){
      $column = 'pl.project_location_code';
    }
    else if($selectfrom == 2){
      $column = 'm.project_id';
    }

    $sql = 'SELECT m.project_id, pl.project_location_code, pl.project_name, m.flow_step,
                   m.project_log, m.meter_log,
                   m.contract_log_old, m.contract_log_new,
                   m.flow_log, m.document_log
            FROM tb_main as m
            INNER JOIN tb_project_log as pl ON pl.log_id = m.project_log
            WHERE '.$column.' = "'.$selectword.'"';
    $result = $mysqli->query($sql);
    if($result->num_rows > 0){
      $out = $result->fetch_array(MYSQLI_ASSOC);

      $flow = array();
      if($out['flow_log'] != 0){
        $sql = 'SELECT * FROM tb_flow_log WHERE log_id = '.$out['flow_log'];
        $result = $mysqli->query($sql);
        $flow = $result->fetch_array(MYSQLI_ASSOC);
      }
      $data = array(
        'bool'          =>  1,
        'data'          =>  $out,
        'f'             =>  $flow
      );
      echo json_encode($data);
    }
    else{
      $data = array(
        'bool'          =>  0
      );
      echo json_encode($data);
    }

    $mysqli->close();
    exit();

?>