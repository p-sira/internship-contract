<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 

    include_once('function.php');
    include_once('dbconnect.php');

    $project_id = $_POST['project_id'];

    $_POST['meter_doc1']        = $_POST['meter_doc1']=='' ? 'NULL' : $_POST['meter_doc1'];
    $_POST['meter_doc2']        = $_POST['meter_doc2']=='' ? 'NULL' : $_POST['meter_doc2'];
    $_POST['meter_check']       = $_POST['meter_check']=='' ? 'NULL' : $_POST['meter_check'];
    $_POST['meter_send']        = $_POST['meter_send']=='' ? 'NULL' : $_POST['meter_send'];
    $_POST['meter_start_date']  = $_POST['meter_start_date']=='' ? 'NULL' : $_POST['meter_start_date'];
    
    $columns = array('project_id', 'meter_number', 'meter_contract_number', 'meter_RP', 'meter_plan',
                     'meter_status', 'meter_doc1', 'meter_doc2', 'meter_send', 'meter_check',
                     'meter_start_date', 'meter_something_number', 'meter_remark', 'meter_who_pay', 
                     'update_person', 'update_datetime');

    $values  = array($_POST['project_id'], $_POST['meter_number'], $_POST['meter_contract_number'],
                     $_POST['meter_RP'], $_POST['meter_plan'], $_POST['meter_status'],
                     $_POST['meter_doc1'], $_POST['meter_doc2'], $_POST['meter_send'],
                     $_POST['meter_check'], $_POST['meter_start_date'], $_POST['meter_something_number'],
                     $_POST['meter_remark'], $_POST['meter_who_pay'], 
                     $_SESSION['userID'], 'CURRENT_TIMESTAMP()');
    
    $insert_id = INSERT('tb_meter_log', $columns, $values);

    $mysqli = dbconnect();
    $sql = 'UPDATE tb_main
            SET meter_log = "'.$insert_id.'"
            WHERE project_id ="'.$project_id.'"';
    $mysqli->query($sql);

    $mysqli->close();
    exit();
?>