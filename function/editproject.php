<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id  = $_POST['project_id'];
    
    $sql = 'INSERT INTO tb_project_log(
                log_id, project_id, project_lot,
                project_number, project_name,
                project_location_code, lot_address,
                lot_person, lot_tel,
                lot_district, lot_status,
                lot_type, lot_detail, date_start, date_finish,
                date_on_service,
                update_person, update_datetime)
            VALUES (NULL,'.$_POST['project_id'].',"'.$_POST['project_lot'].'",
                    "'.$_POST['project_number'].'","'.$_POST['project_name'].'",
                    "'.$_POST['project_location_code'].'","'.$_POST['lot_address'].'",
                    "'.$_POST['lot_person'].'","'.$_POST['lot_tel'].'",
                    "'.$_POST['lot_district'].'","'.$_POST['lot_status'].'",
                    "'.$_POST['lot_type'].'","'.$_POST['lot_detail'].'",
                    "'.$_POST['date_start'].'","'.$_POST['date_finish'].'",
                    "'.$_POST['date_on_service'].'",
                    '.$_SESSION['userID'].', CURRENT_TIMESTAMP())';
    $mysqli->query($sql);
    $insert_id = $mysqli->insert_id;

    $sql = 'UPDATE tb_main
            SET project_log ='.$insert_id.'
            WHERE project_id ='.$project_id;
    $mysqli->query($sql);
    $mysqli->close();
    exit;
?>