<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    $project_id  = $_POST['project_id'];

    include_once('function.php');

    $columns = array('project_id', 'flow1_accept');
    $values = array($project_id, 'CURRENT_TIMESTAMP()');
    $id = insert('tb_flow_log', $columns, $values);

    $columns = array('flow_step', 'flow_log');
    $values = array('1', $id);
    $condition = 'WHERE project_id ='.$project_id;
    update('tb_main', $columns, $values, $condition);

    exit();

?>