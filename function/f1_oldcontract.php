<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');

    $mysqli = dbconnect();

    $log_id = $_POST['log_id'];
    $output = array();

    $columns_c = 'c.log_id, c.contract_number, c.contract_start, c.contract_end,
                c.contract_year, c.contract_type, c.contract_status,
                c.contract_person_name, c.contract_1st_cost, c.contract_annual_cost,
                c.contract_pay_cost, c.contract_other_benefit, 
                c.elec_pay_type, c.elec_pay_rate, c.elec_pay_rate2,
                c.elec_fee, c.elec_fee2,
                c.bank_owner_name, c.bank_code_name, c.bank_number, c.bank_pay_month,
                c.bank_owner_contact1, c.bank_owner_contact2,
                c.node_type, c.node_type2, c.node_step, c.node_child,
                c.mustsendmail';

    $result = $mysqli->query('SELECT '.$columns_c.' FROM tb_contract_log c WHERE c.log_id='.$log_id);
    $output = $result->fetch_array(MYSQLI_ASSOC);
    $mysqli->close();

    $a_contract_type      = getString('contract_type');
    $a_contract_status    = getString('contract_status');
    $a_contract_pay_cost  = getString('contract_pay_cost');
    $a_elec_pay_type      = getString('elec_pay_type');
    $a_elec_pay_rate      = getString('elec_pay_rate');
    $a_elec_fee           = getString('elec_fee');
    $a_month              = getString('month');
    $a_node_type          = getString('node_type');
    $a_node_type2         = getString('node_type2');
    $a_node_step          = getString('node_step');

    $output['contract_status']  = is_numeric($output['contract_status'])  ? $a_contract_status[$output['contract_status']]    : $output['contract_status'];
    $output['contract_type']    = is_numeric($output['contract_type'])    ? $a_contract_type[$output['contract_type']]        : $output['contract_type'];
    $output['contract_pay_cost']= is_numeric($output['contract_pay_cost'])? $a_contract_pay_cost[$output['contract_pay_cost']]: $output['contract_pay_cost'];
    $output['elec_pay_type']    = is_numeric($output['elec_pay_type'])    ? $a_elec_pay_type[$output['elec_pay_type']]        : $output['elec_pay_type'];
    $output['elec_pay_rate']    = is_numeric($output['elec_pay_rate'])    ? $a_elec_pay_rate[$output['elec_pay_rate']]        : $output['elec_pay_rate'];
    $output['elec_fee']         = is_numeric($output['elec_fee'])         ? $a_elec_fee[$output['elec_fee']]                  : $output['elec_fee'];
    $output['elec_pay_rate']    .= ' '.$output['elec_pay_rate2'];
    $output['elec_fee']         .= ' '.$output['elec_fee2'];

    $output['bank_pay_month']   = is_numeric($output['bank_pay_month'])   ? $a_month[$output['bank_pay_month']]   : $output['bank_pay_month'];

    $output['node_type']        = is_numeric($output['node_type'])  ? $a_node_type[$output['node_type']]   : $output['node_type'];
    $output['node_type2']       = is_numeric($output['node_type2']) ? $a_node_type2[$output['node_type2']] : $output['node_type2'];
    $output['node_step']        = is_numeric($output['node_step'])  ? $a_node_step[$output['node_step']]   : $output['node_step'];
    
    $data = array(
        'bool'		=>      1,
        'old'       =>      $output
    );
    echo json_encode($data);
    exit();
   
?>