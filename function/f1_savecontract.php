<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    $project_id  = $_POST['project_id'];

    include_once('function.php');
  
    $columns    = array(
        'project_id', 'contract_number',
        'contract_start', 'contract_end', 'contract_year',
        'contract_type', 'contract_status', 'contract_person_name',
        'contract_1st_cost', 'contract_annual_cost', 'contract_pay_cost',
        'contract_other_benefit', 'elec_pay_type',
        'elec_pay_rate', 'elec_pay_rate2', 'elec_fee', 'elec_fee2',
        'bank_owner_name', 'bank_code_name', 'bank_number', 'bank_pay_month',
        'bank_owner_contact1', 'bank_owner_contact2',
        'node_type', 'node_type2', 'node_step', 'node_child',
        'mustsendmail', 'update_status', 'update_person', 'update_date');
    $values     = array(
        $_POST['project_id'], $_POST['contract_number'],
        $_POST['contract_start'], $_POST['contract_end'],$_POST['contract_year'],
        $_POST['contract_type'],$_POST['contract_status'],$_POST['contract_person_name'],
        $_POST['contract_1st_cost'],$_POST['contract_annual_cost'],$_POST['contract_pay_cost'],
        $_POST['contract_other_benefit'],$_POST['elec_pay_type'],
        $_POST['elec_pay_rate'],$_POST['elec_pay_rate2'],$_POST['elec_fee'],$_POST['elec_fee2'],
        $_POST['bank_owner_name'],$_POST['bank_code_name'],$_POST['bank_number'],$_POST['bank_pay_month'],
        $_POST['bank_owner_contact1'],$_POST['bank_owner_contact2'],
        $_POST['node_type'],$_POST['node_type2'],$_POST['node_step'],$_POST['node_child'],
        $_POST['mustsendmail'], '0', $_SESSION['userID'],'CURRENT_TIMESTAMP()',);

    $insert_cid = insert('tb_contract_log', $columns, $values);   //ใส่ค่าสัญญาใหม่ลงตาราง

    if($_POST['mustsendmail'] == '0'){    ////********************** ถ้าไม่้ต้องส่งเมล ให้ข้ามขั้นอนุมัติเลย

        $columns    = array('flow1_finish', 'flow2_accept', 'flow2_finish', 'flow3_accept');
        $values     = array('CURRENT_TIMESTAMP()', 'CURRENT_TIMESTAMP()', 'CURRENT_TIMESTAMP()', 'CURRENT_TIMESTAMP()');
        $condition  = 'WHERE log_id = (SELECT flow_log FROM tb_main WHERE project_id ='.$project_id.')';
        update('tb_flow_log', $columns, $values, $condition);         // อัพเวลา

        $columns    = array('project_id', 'mail_remark');
        $values     = array($project_id, 'ข้ามขั้นอนุมัติ');
        $doc_id     = insert('tb_document_log', $columns, $values);   // อัพ doc_log, เพราะข้ามขั้น

        $columns    = array('flow_step', 'document_log', 'contract_log_new');
        $values     = array('3', $doc_id, $insert_cid);
        $condition  = 'WHERE project_id = '.$project_id;
        update('tb_main', $columns, $values, $condition);              // อัพ flow_step เป็น 3
    }
    else {                                ////********************** ถ้าต้องส่งเมล ต้องทำขั้นอนุมัติ

        $columns    = array('contract_log_new', 'flow_step');
        $values     = array($insert_cid, '2');
        $condition  = 'WHERE project_id ='.$project_id;
        update('tb_main', $columns, $values, $condition);             // อัพเลข logของสัญญาใหม่, flow_step เป็น 2

        $columns    = array('flow1_finish', 'flow2_accept');
        $values     = array('CURRENT_TIMESTAMP()', 'CURRENT_TIMESTAMP()');
        $condition  = 'WHERE log_id = (SELECT flow_log FROM tb_main WHERE project_id ='.$project_id.')';
        update('tb_flow_log', $columns, $values, $condition);         // อัพเวลา
    }

    exit;

?>