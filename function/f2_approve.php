<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = $_POST['project_id'];
    $remark = isset($_POST['remark']) ? '' : $_POST['remark'];

    //สร้างโฟลเดอ ถ้ายังไม่มี
    $target_dir = '../uploads/mailcopy/';
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }

    
    //อัพเดทดาต้าเบส (ไปขั้นถัดไป + วันเวลาที่ทำขั้นนี้เสร็จ)	
    $sql = 'SELECT 	m.flow_log, m.contract_log_new,
                    fl.flow2_rollback
            FROM 	tb_main m, tb_flow_log fl
            WHERE 	m.flow_log = fl.log_id AND
                    m.project_id = '.$project_id;
    $result = $mysqli->query($sql);
    $output = $result->fetch_array(MYSQLI_ASSOC);

    $columns    = array('project_id', 'mail_remark');
    $values     = array($project_id, $remark);
    $mail_id     = insert('tb_document_log', $columns, $values);

    $columns    = array('flow_step', 'document_log');
    $values     = array('3', $mail_id);
    $condition  = 'WHERE project_id = '.$project_id;
    update('tb_main', $columns, $values, $condition);

    // อนุมัติแล้ว -> ปรับสัญญานี้เป็นล่าสุด (update_status อนุมัติ=1, ยัง=0)
    $columns    = array('update_status');
    $values     = array('1');
    $condition  = 'WHERE log_id ='.$output['contract_log_new'];
    update('tb_contract_log', $columns, $values, $condition);
    
    $columns    = array('flow2_finish' , 'flow3_accept', 'flow2_notapprove');
    $values     = array('CURRENT_TIMESTAMP()', 'CURRENT_TIMESTAMP()', '0');
    $condition  = 'WHERE log_id ='.$output['flow_log'];
    update('tb_flow_log', $columns, $values, $condition);


    if(isset($_FILES['file_email'])) {
        
        // สร้างชื่อไฟล์จาก lot, no, code, วันที่
        $num_mail = date("Y-m-d");
        $sql = 'SELECT project_lot as lot, project_number as no, project_location_code as code
                FROM tb_project_log
                WHERE log_id = (SELECT project_log FROM tb_main WHERE project_id ='.$project_id.' )';
        $result = $mysqli->query($sql);
        $output = $result->fetch_array(MYSQLI_ASSOC);

        // ชื่อไฟล์  uploads/mailcopy/mail_T_T68_0145-37_2017-28-06.png
        $ext = explode('.', $_FILES['file_email']['name']);	//เอานามสกุล
        $target_file = $target_dir
                    .'mail_'.$output['lot']
                    .'_'.$output['no']
                    .'_'.$output['code']
                    .'_'.$num_mail
                    .'.'.end($ext);
        //ไฟล์ซ้ำ อัพทับ
        if (move_uploaded_file($_FILES['file_email']['tmp_name'], $target_file)){		//อัพสำเร็จ
          $columns    = array('mail_file_path', 'update_datetime');
          $values     = array($target_file, 'CURRENT_TIMESTAMP()');
          $condition  = ' WHERE log_id ='.$mail_id;
          update('tb_document_log', $columns, $values, $condition);
        }
        else {
          $data = array(
              'bool'	=>	0,
              'text'	=>	'ไม่อัพ'
          );
          echo json_encode($data);
          $mysqli->close();
          exit;
        }
      $data = array(
        'bool'	=>	1,
        'text'	=>	'อัพไฟล์สำเร็จ'
      );
      echo json_encode($data);
    }
 
    $mysqli->close();
    exit;

?>