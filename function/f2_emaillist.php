<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $email = array('sittha.s@test.com');
    $id = array('');
    $name = array('Sittha Test');

    $sql = 'SELECT user_id, user_email, user_name
            FROM tb_user WHERE in_mail_list = "1"';
    $result = $mysqli->query($sql);
    if($result->num_rows > 0){
      while($out = $result->fetch_array(MYSQLI_ASSOC)){
        $email[]  = $out['user_email'];
        $id[]     = $out['user_id'];
        $name[]   = $out['user_name'];
      }
    }

    $data = array(
      'email'   => $email,
      'id'      => $id,
      'name'    => $name
    );

    $mysqli->close();
    echo json_encode($data);
    exit();

?>