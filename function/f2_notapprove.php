<?php


    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = $_POST['project_id'];

    //สร้างโฟลเดอ ถ้ายังไม่มี
    $target_dir = '../uploads/mailcopy/';
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }

    $columns    = array('project_id', 'mail_remark');
    $values     = array($project_id, $_POST['remark']);
    $doc_id     = insert('tb_document_log', $columns, $values);

    $columns    = array('document_log');
    $values     = array($doc_id);
    $condition  = 'WHERE project_id = '.$project_id;
    update('tb_main', $columns, $values, $condition);

    //อัพเดทดาต้าเบส ยกเลิกโหนด
    $columns    = array('lot_status');
    $values     = array('1');
    $condition  = 'WHERE log_id = (SELECT project_log FROM tb_main WHERE project_id = '.$project_id.')';
    update('tb_project_log', $columns, $values, $condition);

    $columns    = array('flow2_finish' , 'flow2_notapprove');
    $values     = array('CURRENT_TIMESTAMP()', '1');
    $condition  = 'WHERE log_id = (SELECT flow_log FROM tb_main WHERE project_id = '.$project_id.')';
    update('tb_flow_log', $columns, $values, $condition);

    if(isset($_FILES['file_email'])) {

      // สร้างชื่อไฟล์จาก lot, no, code, วันที่
      $num_mail = date("Y-m-d");
      $sql = 'SELECT project_lot as lot, project_number as no, project_location_code as code
              FROM tb_project_log
              WHERE log_id = (SELECT project_log FROM tb_main WHERE project_id ='.$project_id.' )';
      $result = $mysqli->query($sql);
      $output = $result->fetch_array(MYSQLI_ASSOC);

      // ชื่อไฟล์  uploads/mailcopy/mail_T_T68_0145-37_2017-28-06.png
      $ext = explode('.', $_FILES['file_email']['name']);	//เอานามสกุล
      $target_file = $target_dir
                  .'mail_'.$output['lot']
                  .'_'.$output['no']
                  .'_'.$output['code']
                  .'_'.$num_mail
                  .'.'.end($ext);
      
      //ไฟล์ซ้ำ อัพทับ
      if (move_uploaded_file($_FILES['file_email']['tmp_name'], $target_file)){		//อัพสำเร็จ
        $sql = 'UPDATE tb_document_log
                SET mail_file_path ="'.$target_file.'"
                WHERE log_id ='.$output['document_log'];
        $mysqli->query($sql);
        $columns    = array('mail_file_path', 'update_datetime');
        $values     = array($target_file, 'CURRENT_TIMESTAMP()');
        $condition  = 'WHERE log_id ='.$doc_id;
        update('tb_document_log', $columns, $values, $condition);
      }
      else {
        $data = array(
            'bool'	=>	0,
            'text'	=>	'ไม่อัพ'
        );
        echo json_encode($data);
        $mysqli->close();
        exit;
      }
        
    }
    
    $data = array(
        'bool'	=>	1,
        'text'	=>	'อัพสำเร็จ'
    );
    echo json_encode($data);
        
    $mysqli->close();
    exit;	

?>