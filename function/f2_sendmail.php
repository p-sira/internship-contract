<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('function.php');
    include_once('dbconnect.php');
    require '../assets/PHPMailer/PHPMailerAutoload.php';

    $mysqli = dbconnect();

    //ตัวแปรจาก POST
    $project_id   = $_POST['project_id'];
    $mail_to      = $_POST['mail_to'];
    $mail_cc      = $_POST['mail_cc'];
    $mail_subject = $_POST['mail_subject'];
    $mail_detail  = $_POST['mail_detail'];

    $user = getUserDetail($_SESSION['userID']);

    //บังคับส่ง
    $RO10mail = 'darroct.blues@gmail.com';  //เมล RO10
    //$dimail = 'sittha.s@jasmine.com'      //เมล ผอ.

    $mailer             = new PHPMailer();
    $mailer->IsSMTP();
    $mailer->SMTPDebug  = 0;
    $mailer->SMTPAuth   = true;
    $mailer->SMTPSecure = "tls";
    $mailer->Host       = "smtp.gmail.com";
    $mailer->Port       = 587;
    $mailer->Username   = "rx.portal@gmail.com";
    $mailer->Password   = "21482149";
    
    //ส่งให้ใคร แยกจาก ;
    $a_mail_to = explode(';',$mail_to);
    foreach ($a_mail_to as $i => $mail) {
        $mailer->AddAddress($mail);
    }
    //$mailer->AddAddress($dimail);      //เมล ผอ.
    
    //ccใครบ้าง แยกจาก ','
    $a_mail_cc = explode(',',$mail_cc);
    foreach ($a_mail_cc as $i => $mail) {
        $mailer->AddCC($mail);
    }
    $mailer->AddCC($RO10mail);   //ccหา RO10 ด้วย    

    $mailer->setFrom($user['user_email'], $user['user_name']); //ตัวเองเป็นผู้ส่ง
    $mailer->AddCC($user['user_email'], $user['user_name']);   //ccตัวเองด้วย
    $mailer->CharSet 	= "utf-8";
    $mailer->IsHTML(true);
    $mailer->Subject 	= $mail_subject;
    $mailer->Body       = $mail_detail;

    if(!$mailer->Send()) {
      $data = array(
          'bool' => 0,
          'text' => $mailer->ErrorInfo
      );
      echo json_encode($data);
    }
    else {
      $columns    = array('flow2_sendmail');
      $values     = array('1');
      $condition  = 'WHERE log_id = (SELECT flow_log FROM tb_main WHERE project_id='.$project_id.')';
      update('tb_flow_log', $columns, $values, $condition);

      $data = array(
          'bool' => 1,
          'text' => 'ส่งแล้ว'
      );
      echo json_encode($data);
    }
    $mysqli->close();
    exit;

?>