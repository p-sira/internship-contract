<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = $_POST['project_id'];

    $columns    = array('project_id', 'mail_remark', 'mail_reject', 'update_datetime');
    $values     = array($project_id, $_POST['remark'], '1', 'CURRENT_TIMESTAMP()');
    $doc_id     = insert('tb_document_log', $columns, $values);
    
    $columns    = array('flow_step', 'document_log');
    $values     = array('-1', $doc_id);
    $condition  = 'WHERE project_id = '.$project_id;
    update('tb_main', $columns, $values, $condition);

    $columns    = array('flow2_rollback', 'flow2_sendmail', 'flow2_notapprove', 'flow1_finish', 'flow2_finish');
    $values     = array('1','0', '0', 'NULL', 'CURRENT_TIMESTAMP()');
    $condition  = 'log_id = (SELECT flow_log FROM tb_main WHERE project_id = '.$project_id.')';
    update('tb_flow_log', $columns, $values, $condition);


    // อนุมัติแล้ว -> ปรับสัญญานี้เป็นล่าสุด (อนุมัติ=1, ยัง=0, ยกเลิก=2)
    $columns    = array('update_status');
    $values     = array('2');
    $condition  = 'WHERE log_id = (SELECT contract_log_new FROM tb_main WHERE project_id = '.$project_id.')';
    update('tb_contract_log', $columns, $values, $condition);

    $mysqli->close();
    exit;

?>