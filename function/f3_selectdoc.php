<?php
    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = $_POST['project_id'];
    $doc_type = $_POST['doc_type'];

    $sql = 'SELECT flow_log, document_log  FROM tb_main
            WHERE project_id= '.$project_id;
    $result = $mysqli->query($sql);
    $output = $result->fetch_array(MYSQLI_ASSOC);
    
    $sql = 'UPDATE tb_document_log
            SET update_datetime = CURRENT_TIMESTAMP(),
                doc_type ="'.$doc_type.'"
            WHERE log_id ='.$output['document_log'];        
    $mysqli->query($sql);

    $sql = 'UPDATE tb_flow_log
            SET flow3_finish = CURRENT_TIMESTAMP(),
                flow4_accept = CURRENT_TIMESTAMP()
            WHERE log_id ='.$output['flow_log'];
    $mysqli->query($sql);

    $sql = 'UPDATE tb_main
            SET flow_step = 4
            WHERE project_id = '.$project_id;
    $mysqli->query($sql);

    $mysqli->close();
    exit;

?>