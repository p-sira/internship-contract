<?php
    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id     = $_POST['project_id'];
    $documents      = $_POST['documents']; 
    $doc_remark     = $_POST['doc_remark'];

    $sql = 'SELECT d.doc_reject, m.document_log
            FROM tb_main m
            INNER JOIN tb_document_log d ON d.log_id = m.document_log
            WHERE m.project_id= '.$project_id;
    $result = $mysqli->query($sql);
    $output = $result->fetch_array(MYSQLI_ASSOC);

    // doc_reject++ (ปฏิเสธมากี่ครั้งแล้ว)
    $sql = 'UPDATE tb_document_log
            SET doc = "'.$documents.'",
                recieve_date = CURRENT_TIMESTAMP(),
                doc_remark ="'.$doc_remark.'",
                doc_file_path=NULL,
                doc_reject = 1 + '.$output['doc_reject'].',
                update_person = "'.$_SESSION['userID'].'"
            WHERE log_id ='.$output['document_log'];
    $mysqli->query($sql);

    $sql = 'UPDATE tb_main
            SET flow_step = -3
            WHERE project_id = '.$project_id;
    $mysqli->query($sql);

    $mysqli->close();
    exit;

?>