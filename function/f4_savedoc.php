<?php
    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id     = $_POST['project_id'];
    $documents      = $_POST['documents'];       //(0,1,0,1)
    $doc_remark     = $_POST['doc_remark'];
    $target_file    = '';
    $target_dir     = '../uploads/doccopy/';

    //สร้างโฟลเดอ ถ้ายังไม่มี
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }

    if(isset($_FILES['file_doc'])) {
              
      // เอาชื่อไฟล์จาก lot, no, code, วันที่
      $doc_num = date("Y-m-d");
      $sql = 'SELECT project_lot as lot, project_number as no, project_location_code as code
              FROM tb_project_log
              WHERE log_id = (SELECT project_log FROM tb_main WHERE project_id ='.$project_id.' )';
      $result = $mysqli->query($sql);
      $output = $result->fetch_array(MYSQLI_ASSOC);

      ////////////////////////////////////////////////////////////////////////////////////////////////////////

      // ชื่อไฟล์  uploads/doccopy/doc_T_T68_0145-37_2017-28-06.pdf
      $ext = explode('.', $_FILES['file_doc']['name']);	//เอานามสกุล
      $target_file = $target_dir
                  .'doc_'.$output['lot']
                  .'_'.$output['no']
                  .'_'.$output['code']
                  .'_'.$doc_num
                  .'.'.end($ext);

      $sql = 'UPDATE tb_document_log
              SET doc = "'.$documents.'",
                  recieve_date = CURRENT_TIMESTAMP(),
                  doc_remark ="'.$doc_remark.'", doc_file_path="'.$target_file.'",
                  update_person = "'.$_SESSION['userID'].'"
              WHERE log_id =
                  (SELECT document_log FROM tb_main WHERE project_id= '.$project_id.')';
      $mysqli->query($sql);

      $sql = 'UPDATE tb_main
              SET flow_step = 5
              WHERE project_id = '.$project_id;
      $mysqli->query($sql);

      $sql = 'UPDATE tb_flow_log
              SET flow4_finish = CURRENT_TIMESTAMP(),
                  flow5_accept = CURRENT_TIMESTAMP()
              WHERE log_id = (SELECT flow_log FROM tb_main WHERE project_id= '.$project_id.')';
      $mysqli->query($sql);
      $mysqli->close();

      if(move_uploaded_file($_FILES['file_doc']['tmp_name'], $target_file)) {
          $data = array(
              'bool'	=>	1,
              'text'	=>	'อัพ'
          );
          echo json_encode($data);
      } else {
          $data = array(
              'bool'	=>	0,
              'text'	=>	'ไม่อัพ'
          );
          echo json_encode($data);
      }
    }
    exit;

?>