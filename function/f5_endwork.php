<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = 2689;
    
    $sql = 'UPDATE tb_contract_log SET update_status = 1
            WHERE log_id = (SELECT contract_log_new FROM tb_main WHERE project_id ='.$project_id.')';
    $mysqli->query($sql);
    $sql = 'UPDATE tb_flow_log 
            SET flow5_finish = CURRENT_TIMESTAMP()
            WHERE log_id = (SELECT flow_log FROM tb_main WHERE project_id ='.$project_id.')';
    $mysqli->query($sql);	

    $sql = 'INSERT INTO tb_grand_log
            (
                project_id, project_log, meter_log,
                contract_log_old, contract_log_new,
                flow_log, document_log,
                update_datetime, status, flow_step
            )
            SELECT  project_id, project_log, meter_log,
                    contract_log_old, contract_log_new,
                    flow_log, document_log,
                    CURRENT_TIMESTAMP(), "สำเร็จ", flow_step
            FROM tb_main
            WHERE project_id ='.$project_id;
    $mysqli->query($sql);

    $sql = 'UPDATE tb_main
            SET flow_step = 0, contract_log_old = contract_log_new
            WHERE project_id ='.$project_id;
    $mysqli->query($sql);

    $mysqli->close();
    exit();
  
?>