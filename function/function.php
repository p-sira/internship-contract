<?php

  include_once('dbconnect.php');

  function getString($column) {
    $mysqli = dbconnect();
    $sql = 'SELECT `'.$column.'` 
            FROM tb_string
            WHERE `'.$column.'` IS NOT NULL';
    $result = $mysqli->query($sql);
    $data = array('0');
    if ($result->num_rows > 0) {
        while($output = $result->fetch_array(MYSQLI_ASSOC)){
            $data[] = $output[$column];
        }
    }
    else{
        $data = 'error';
    }
    $mysqli->close();
    return $data;
  }

  function getUserDetail($user_id) {
    $mysqli = dbconnect();
    $sql = 'SELECT * FROM tb_user WHERE user_id ="'.$user_id.'"';
    $result = $mysqli->query($sql);

    if ($result->num_rows > 0) {
        $output = $result->fetch_array(MYSQLI_ASSOC);
        $data = $output;
    }
    else{
        $data = 'error';
    }
    $mysqli->close();
    return $data;
  }

  //***********************************************
  //  $table เช่น 'tb_main'
  //  $columns + $values เป็น อาเรย์
  //    รีเทิน id ที่ใส่เข้าไป (insert_id)
  //***********************************************
  function insert($table, $columns, $values){
    $mysqli = dbconnect();
    $in_columns = '(';
    foreach ($columns as $key => $column) {
        if($key != 0)
          $in_columns .= ',';
        $in_columns .= $column;
      }
      $in_columns .= ')';


    $in_values = '(';
    foreach ($values as $key => $value) {
        if($key != 0)
          $in_values .= ',';
        if($value == 'NULL' || $value == 'CURRENT_TIMESTAMP()')
          $in_values .= $value;
        else
          $in_values .= '"'.$value.'"';
      }
      $in_values .= ')';


    if($columns == '')
      $sql = 'INSERT INTO '.$table.'
              VALUES'.$in_values.';';
    else
      $sql = 'INSERT INTO '.$table.$in_columns.'
              VALUES'.$in_values.';';

    $mysqli->query($sql);
    $insert_id = $mysqli->insert_id;
    $mysqli->close();

    return $insert_id;
  }

  //***********************************************
  //  $table เช่น 'tb_main'
  //  $columns + $values เป็น อาเรย์
  //***********************************************
  function update($table, $columns, $values, $condition){
    $mysqli = dbconnect();
    $set = '';
    foreach ($columns as $i => $x) {

        if($i != 0)
          $set .= ',';
        if($values[$i] == 'NULL' || $values[$i] == 'CURRENT_TIMESTAMP()')
          $set .= $columns[$i].'='.$values[$i].' ';
        else
          $set .= $columns[$i].'='.'"'.$values[$i].'" ';
      }
    $sql = 'UPDATE '.$table.' SET '.$set.' '.$condition.';';
    $mysqli->query($sql);
    $status = $mysqli->affected_rows;
    $mysqli->close();

    return $status;
  }
  
?>