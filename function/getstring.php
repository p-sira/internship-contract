<?php
 
  
  $columns = explode(' ',$_GET['column']);       //ในลิ้ง ให้เชื่อมด้วย +
  $json_data = array();

  include_once('dbconnect.php');
  $mysqli = dbconnect();

  $sql = 'SELECT string_id ';
  foreach ($columns as $column) {
      $sql.= ','.$column.' ';
  }
  $sql.= 'FROM tb_string;';
      
  $result = $mysqli->query($sql);
  $rows = $result->num_rows;

  if(isset($_GET['forarray'])){  //มี dummy เพิ่มมาในช่องที่ 0 เพราะ id จริงเริ่มจาก 1 แต่อาเรย์เริ่มจาก 0
      $json_data[] = '';
  }

  if($rows > 0){
    while($output = $result->fetch_array(MYSQLI_ASSOC)){
      $data = array();
      $data['id']         =  $output['string_id'];
      foreach ($columns as $column) {
          $data[$column]  =  $output[$column];
      }
      $json_data[] = $data;
    }
  }
  echo json_encode($json_data);
  $mysqli->close();
  exit();

?>