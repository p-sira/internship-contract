<?php

  date_default_timezone_set('Asia/Bangkok');
  if(!isset($_SESSION)) { 
    session_start(); 
  } 

  $project_id  = $_POST['project_id'];
  include_once('dbconnect.php');
  $mysqli = dbconnect();

  $columns_m = 'm.project_id, m.flow_step,
                m.project_log, m.meter_log,
                m.contract_log_old, m.contract_log_new,
                m.flow_log, m.document_log';

  $columns_c = 'c.log_id, c.contract_number, c.contract_start, c.contract_end,
                c.contract_year, c.contract_type, c.contract_status,
                c.contract_person_name, c.contract_1st_cost, c.contract_annual_cost,
                c.contract_pay_cost, c.contract_other_benefit, 
                c.elec_pay_type, c.elec_pay_rate, c.elec_pay_rate2,
                c.elec_fee, c.elec_fee2,
                c.bank_owner_name, c.bank_code_name, c.bank_number, c.bank_pay_month,
                c.bank_owner_contact1, c.bank_owner_contact2,
                c.node_type, c.node_type2, c.node_step, c.node_child,
                c.mustsendmail';

  $columns_d = 'd.log_id, d.mail_remark, d.mail_file_path,
                d.doc, d.doc_type, d.doc_remark, d.doc_file_path, d.recieve_date';

  $columns_me= 'me.meter_number, me.meter_contract_number, me.meter_RP, me.meter_plan,
                me.meter_status, me.meter_doc1, me.meter_doc2, me.meter_send, me.meter_check,
                me.meter_start_date, me.meter_something_number, me.meter_remark, me.meter_who_pay';

  $columns_p = 'p.project_lot, p.project_number, p.project_name, p.project_location_code,
                p.lot_address, p.lot_person, p.lot_tel, p.lot_district, p.lot_status,
                p.lot_type, p.lot_detail, p.date_start, p.date_finish, p.date_on_service';

  $columns_f = 'f.flow1_accept, f.flow1_finish,
                f.flow2_accept, f.flow2_finish, f.flow2_rollback, f.flow2_sendmail, f.flow2_notapprove,
                f.flow3_accept, f.flow3_finish,
                f.flow4_accept, f.flow4_finish, f.flow4_rollback,
                f.flow5_accept, f.flow5_finish';

  $sql = 'SELECT '.$columns_m.' FROM tb_main m WHERE m.project_id='.$project_id;
  $result = $mysqli->query($sql);
  if($result->num_rows == 0) {
    $mysqli->close();
    $data = array(
        'bool'		=> 0
    );
    echo json_encode($data);
  }
  else{
    $m = $result->fetch_array(MYSQLI_ASSOC);

    $co = array('bool' => 0);
    if($m['contract_log_old'] != NULL){
      $sql    = 'SELECT '.$columns_c.' FROM tb_contract_log c WHERE c.log_id='.$m['contract_log_old'];
      $result = $mysqli->query($sql);
      $co     = $result->fetch_array(MYSQLI_ASSOC);
    }
    $cn = array('bool' => 0);
    if($m['contract_log_new'] != NULL){
      $sql    = 'SELECT '.$columns_c.' FROM tb_contract_log c WHERE c.log_id='.$m['contract_log_new'];
      $result = $mysqli->query($sql);
      $cn     = $result->fetch_array(MYSQLI_ASSOC);
    }
    $d = array('bool' => 0);
    if($m['document_log'] != NULL){
      $sql    = 'SELECT '.$columns_d.' FROM tb_document_log d WHERE d.log_id='.$m['document_log'];
      $result = $mysqli->query($sql);
      $d      = $result->fetch_array(MYSQLI_ASSOC);
    }
    $p = array('bool' => 0);
    if($m['project_log'] != NULL){
      $sql    = 'SELECT '.$columns_p.' FROM tb_project_log p WHERE p.log_id='.$m['project_log'];
      $result = $mysqli->query($sql);
      $p      = $result->fetch_array(MYSQLI_ASSOC);
    }
    $me = array('bool' => 0);
    if($m['meter_log'] != NULL){
      $sql    = 'SELECT '.$columns_me.' FROM tb_meter_log me WHERE me.log_id='.$m['meter_log'];
      $result = $mysqli->query($sql);
      $me     = $result->fetch_array(MYSQLI_ASSOC);
    }
    $f = array('bool' => 0);
    if($m['flow_log'] != NULL){
      $sql    = 'SELECT '.$columns_f.' FROM tb_flow_log f WHERE f.log_id='.$m['flow_log'];
      $result = $mysqli->query($sql);
      $f      = $result->fetch_array(MYSQLI_ASSOC);
    }

    $oldlog = array();
    $sql = 'SELECT * FROM tb_contract_log 
            WHERE  (update_status != 0 OR update_status IS NULL) 
            AND project_id ='.$project_id;
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      while ($output = $result->fetch_array(MYSQLI_ASSOC)) {
        if($output['update_date'] == null)
          $output['update_date'] = '0000-00-00 00:00:00';
        
        $oldlog[] = array(
          'option_value'	=>	$output['log_id'],
          'option_text'	  =>	$output['update_date']
        );
      }
    }

    $mysqli->close();
    $data = array(
        'bool'	=> 1,
        'm'     => $m,
        'f'     => $f,
        'p'     => $p,
        'me'    => $me,
        'd'     => $d,
        'co'    => $co,
        'cn'    => $cn,
        'oldlog'=> $oldlog
    );
    echo json_encode($data);
  }
  exit();

?>