<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = $_POST['project_id'];

    $sql = 'SELECT  pl.project_name, pl.project_location_code 
            FROM    tb_main m, tb_project_log pl
            WHERE 	(m.project_log = pl.log_id) AND 
                     m.project_id = '.$project_id;
    $result = $mysqli->query($sql);
    if($result->num_rows == 0) {
        $mysqli->close();
        $data = array(
            'bool'		=> 0
        );

        echo json_encode($data);
    }
    else{
        $project = $result->fetch_array(MYSQLI_ASSOC);
        $sql = 'SELECT * 
            FROM tb_main m, tb_meter_log ml
            WHERE 	(m.meter_log = ml.log_id) AND 
                     m.project_id = '.$project_id;

        $result = $mysqli->query($sql);
        
        $output = $result->fetch_array(MYSQLI_ASSOC);
            $data = array(
            'bool'                  => 1,
            'project_id'            => $output['project_id'],
            'project_name'		    => $project['project_name'],
            'project_location_code'	=> $project['project_location_code'],
            'meter_number'          => $output['meter_number'],
            'meter_contract_number' => $output['meter_contract_number'],
            'meter_RP'              => $output['meter_RP'],
            'meter_plan'            => $output['meter_plan'],
            'meter_status'          => $output['meter_status'],
            'meter_doc1'            => $output['meter_doc1'],
            'meter_doc2'            => $output['meter_doc2'],
            'meter_send'            => $output['meter_send'],
            'meter_check'           => $output['meter_check'],
            'meter_start_date'      => $output['meter_start_date'],
            'meter_something_number'=> $output['meter_something_number'],
            'meter_remark'          => $output['meter_remark'],
            'meter_who_pay'         => $output['meter_who_pay'],
            'update_person'         => $output['update_person'],
            'update_datetime'       => $output['update_datetime']
        );
        echo json_encode($data);
    }
    $mysqli->close();
    exit;
?>