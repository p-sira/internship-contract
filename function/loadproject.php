<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = $_POST['project_id'];

    $sql = 'SELECT * 
            FROM    tb_main m, tb_project_log pl
            WHERE 	(m.project_log = pl.log_id) AND 
                     m.project_id = '.$project_id;

    $result = $mysqli->query($sql);
    if($result->num_rows == 0) {
      $mysqli->close();
          $data = array(
          'bool'		=> 0
      );
      echo json_encode($data);
    }
    else {
      $output = $result->fetch_array(MYSQLI_ASSOC);
        $data = array(
          'bool'                  => 1,
          'project_id'            => $output['project_id'],
          'project_lot'           => $output['project_lot'],
          'project_number'        => $output['project_number'],
          'project_name'          => $output['project_name'],
          'project_location_code' => $output['project_location_code'],
          'lot_address'           => $output['lot_address'],
          'lot_person'            => $output['lot_person'],
          'lot_tel'               => $output['lot_tel'],
          'lot_district'          => $output['lot_district'],
          'lot_status'            => $output['lot_status'],
          'lot_type'              => $output['lot_type'],
          'date_start'            => $output['date_start'],
          'date_finish'           => $output['date_finish'],
          'date_on_service'       => $output['date_on_service'],
          'update_person'         => $output['update_person'],
          'update_datetime'       => $output['update_datetime']
        );
        echo json_encode($data);
    }
    $mysqli->close();
    exit;
?>