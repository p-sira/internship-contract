<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $year = $_GET['year'];
    $a_month = getString('month');
    //node_type = [ '', '-', 'ยกเลิก', 'DSLAM', 'ME', 'OLT', 'KC', 'FT']
    // lot_type = ['หน่วยงาน', 'ชุมชน', 'คอนโด', 'ที่พัก', 'หมู่บ้าน']

    $wh_lot_type = array(
        ' AND (pl.lot_type = 1 OR pl.lot_type = 2) ',
        ' AND (pl.lot_type = 3) ',
        ' AND (pl.lot_type = 4 OR pl.lot_type = 5) '
    );

    $wh_node_type= array(
        array(
            ' AND (cl.node_type = 1) ',
            ' AND (cl.node_type = 2) ',
            ' AND (cl.node_type = 3) ',
            ' AND ((cl.node_type!=1 AND cl.node_type!=2 AND cl.node_type!=3) OR cl.node_type IS NULL)'
        ),

        array(
            ' AND (cl.node_type = 1) ',
            ' AND (cl.node_type = 4) ',
            ' AND (cl.node_type = 5) ',
            ' AND ((cl.node_type!=1 AND cl.node_type!=4 AND cl.node_type!=5) OR cl.node_type IS NULL)'
        ),

        array(
            ' AND (cl.node_type = 1) ',
            ' AND (cl.node_type = 2) ',
            ' AND (cl.node_type = 3) ',
            ' AND ((cl.node_type!=1 AND cl.node_type!=2 AND cl.node_type!=3) OR cl.node_type IS NULL)'
        )
    );
    $length = array(
        count($wh_node_type[0]),
        count($wh_node_type[1]),
        count($wh_node_type[2])
    );

    $mainsql = 'SELECT count(a.project_id) as count
                FROM(
                    SELECT m.project_id, m.project_log, m.contract_log_new
                    FROM tb_main as m
                    WHERE m.flow_step >= 2 
                    
                    UNION

                    SELECT  g.project_id, g.project_log, g.contract_log_new
                    FROM tb_grand_log as g
                ) a
                LEFT JOIN tb_contract_log cl ON a.contract_log_new = cl.log_id
                LEFT JOIN tb_project_log pl ON a.project_log = pl.log_id

                WHERE cl.contract_status = 1 AND (cl.node_step IS NULL OR cl.node_step != 2)
                    AND pl.lot_status != 1
                    AND YEAR(cl.contract_end) = "'.$year.'" ';

    $json_data = array();
    $button_data = array();

    for ($month=1; $month <= 12; $month++) { 
        $nested_data = array();
        $contractCount = 0;
        $monthsql = $mainsql . ' AND MONTH(cl.contract_end) ="'.$month.'" ';
        
        $nested_data[] = $a_month[$month];

        for ($i=0; $i < 3; $i++) { // หน่วยงาน+ชุมชน, คอนโด, ที่พัก+หมู่บ้าน
            $typesql = $monthsql . $wh_lot_type[$i];

            for ($j=0; $j < $length[$i]; $j++) { 
                $p_id = array();
                $sql = $typesql . $wh_node_type[$i][$j];

                $result = $mysqli->query($sql);
                $output = $result->fetch_array(MYSQLI_ASSOC);

                $contractCount +=$output['count'];
                $nested_data[] = $output['count'];
            }
        }
        $nested_data[] = $contractCount;

        $json_data[]   = $nested_data;
    }
    $mysqli->close();
    $data = array(
        'data'  => $json_data
    );
    echo json_encode($data);
    exit();

?>