<?php

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $year = $_GET['year'];
    $a_month = getString('month');
    $wh_lot_type = array(
        ' AND (pl.lot_type = 1 OR pl.lot_type = 2) ',
        ' AND (pl.lot_type = 3) ',
        ' AND (pl.lot_type = 4 OR pl.lot_type = 5) '
    );

    $mainsql = 'SELECT count(a.project_id) as count
                FROM(
                    SELECT m.project_id, m.project_log, m.contract_log_new, m.contract_log_old,
                            m.flow_step, m.flow_log
                    FROM tb_main m
                    WHERE m.flow_step >= 2 
                    
                    UNION

                    SELECT  g.project_id, g.project_log, g.contract_log_new, g.contract_log_old,
                            g.flow_step, g.flow_log
                    FROM tb_grand_log as g
                ) a
                LEFT JOIN tb_contract_log cl    ON a.contract_log_new = cl.log_id
                LEFT JOIN tb_contract_log clo   ON a.contract_log_old = clo.log_id
                LEFT JOIN tb_project_log pl     ON a.project_log = pl.log_id
                LEFT JOIN tb_flow_log fl        ON a.flow_log = fl.log_id

                WHERE cl.contract_status = 1 
                    AND (cl.node_step IS NULL OR cl.node_step != 2)
                    AND YEAR(clo.contract_end) = "'.$year.'" ';
    
    $json_data = array();

    for ($month=1; $month <= 12; $month++) { 
        $nested_data = array();
        $all = 0;
        $fin = 0;
        $endcount = 0;
        $monthsql = $mainsql . ' AND MONTH(clo.contract_end) ="'.$month.'" ';

        $nested_data[] = $a_month[$month];

        for ($i=0; $i < 3; $i++) { // หน่วยงาน+ชุมชน, คอนโด, ที่พัก+หมู่บ้าน
            $allsql = $monthsql . $wh_lot_type[$i];                         //ต่อเสร็จแล้ว flow_step >= 4
            $finsql = $allsql . 'AND (a.flow_step >= 4 OR fl.flow2_notapprove = "1")';

            $result  = $mysqli->query($allsql.' AND pl.lot_status != "1"');
            $output1 = $result->fetch_array(MYSQLI_ASSOC);
            $result  = $mysqli->query($finsql.' AND pl.lot_status != "1"' );
            $output2 = $result->fetch_array(MYSQLI_ASSOC);

            $nested_data[] = $output1['count'];
            $nested_data[] = $output2['count'];
            $nested_data[] = $output1['count']==0 ? '-' : round(($output2['count']*100/$output1['count']),2).'%';
            
            $tempsql    = $allsql . ' AND (fl.flow2_notapprove = "1")';
            $result     = $mysqli->query($tempsql);
            $output     = $result->fetch_array(MYSQLI_ASSOC);
            $endcount   += $output['count'];                                // ไม่ต่อสัญญา(โหนดที่ยกเลิก)   

            $all += $output1['count']+$output['count'];
            $fin += $output2['count'];                              
        }
        $nested_data[] = $all;
        $nested_data[] = $fin;
        $nested_data[] = $endcount;
        $nested_data[] = $all==0 ? '-' : round((($endcount+$fin)*100/$all),2).'%';
        $json_data[] = $nested_data;
    }
    $mysqli->close();
    $data = array(
        'data'  => $json_data
    );

    echo json_encode($data);
    exit();

?>