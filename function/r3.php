<?php

    date_default_timezone_set('Asia/Bangkok');
    $today_datetime = new DateTime("now");

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $year = $_GET['year'];
    $a_month = getString('month');
    $json_data = array();

    // คิวรี่ เอาเฉพาะสัญญาปลายปิด, กับ โหนดหลัก
    $mainsql =
        'SELECT count(a.project_id) as count
         FROM(
            SELECT
                m.project_id, m.flow_step,
                m.project_log, m.meter_log,
                m.contract_log_old, m.contract_log_new,
                m.flow_log, m.document_log
            FROM
                tb_main m
            UNION
            SELECT
                g.project_id, g.flow_step,
                g.project_log, g.meter_log,
                g.contract_log_old, g.contract_log_new,
                g.flow_log, g.document_log
            FROM
                tb_grand_log g
        ) a
        LEFT JOIN tb_contract_log cl    ON a.contract_log_new = cl.log_id
        LEFT JOIN tb_contract_log clo   ON a.contract_log_old = clo.log_id
        LEFT JOIN tb_project_log pl     ON a.project_log = pl.log_id
        LEFT JOIN tb_flow_log fl        ON a.flow_log = fl.log_id
        WHERE   cl.contract_status = 1
                AND (cl.node_step IS NULL OR cl.node_step != 2)
                AND YEAR (clo.contract_end) = "'.$year.'"';

    for ($month=1; $month <= 12; $month++) { 
        $monthsql = $mainsql . ' AND MONTH(clo.contract_end) ="'.$month.'" ';

        $nested_data = array();
        $nested_data[] = $a_month[$month];                                          //เดือน

        $tempsql    = $monthsql;
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $allcount   = $output['count'];
        $nested_data[] = $allcount;                                                 //จำนวนทั้งหมด(ไม่เอาโหนดที่ยกเลิก)

        $tempsql    = $monthsql . ' AND a.flow_step = 5 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $fincount   = $output['count'];
        $nested_data[] = $fincount;                                                 // ต่อเสร็จ

        $nested_data[] = $allcount==0? 0: round(($fincount*100/$allcount),2).'%';   // %

        $tempsql    = $monthsql . ' AND fl.flow2_notapprove = 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $endcount   = $output['count'];
        $nested_data[] = $endcount;                                                 // ไม่ต่อสัญญา(โหนดที่ยกเลิก)

        $stillcount = $allcount-$fincount-$endcount;
        $nested_data[] = $stillcount;                                               // รอดำเนินการ

        $nested_data[] = $allcount==0? 0: round(($stillcount*100/$allcount),2).'%'; // %

        $tempsql    = $monthsql . ' AND a.flow_step = 0 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f0count    = $output['count'];
        $nested_data[] = $f0count;                                                  // รอรับงาน flow=0 ()

        $tempsql    = $monthsql . ' AND a.flow_step = 1 OR a.flow_step = -1 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f1count    = $output['count'];
        $nested_data[] = $f1count;                                                  // ติดต่อเจรจา flow = 1

        $tempsql    = $monthsql . ' AND a.flow_step = 2 AND cl.mustsendmail = 1 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f2mcount   = $output['count'];
        $nested_data[] = $f2mcount;                                                 // ต้องเมล = 2

        $tempsql    = $monthsql . ' AND a.flow_step = 2 AND cl.mustsendmail = 1 AND fl.flow2_sendmail = 1 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $nested_data[] = $output['count'];                                          // ส่งเมลแล้ว

        // $tempsql    = $monthsql . ' AND a.flow_step = 2 AND cl.mustsendmail = 0 AND pl.lot_status != 1';
        // $result     = $mysqli->query($tempsql);
        // $output     = $result->fetch_array(MYSQLI_ASSOC);
        // $f2count    = $output['count'];
        // $nested_data[] = $f2count;                                                  // ไม่ต้องส่งเมล = 2


        $tempsql    = $monthsql . ' AND (a.flow_step >= 3 OR a.flow_step = -3) AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $approvecount = $output['count'];
        $nested_data[] = $approvecount;                                            // อนุมัตต่อสัญญาแล้ว flow 3 -3 4 5

        $nested_data[] = $endcount;                                                // ไม่ต่อสัญญา

        $tempsql    = $monthsql . ' AND a.flow_step = -1 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f1count2   = $output['count'];
        $nested_data[] = $f1count2;                                                // เจรจาเพิ่ม flow= -1

        $tempsql    = $monthsql . ' AND a.flow_step = 2 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f2count    = $output['count'];
        $nested_data[] = $f2count;                                                // คงค้างอนุมัต

        $tempsql    = $monthsql . ' AND a.flow_step = 3 OR a.flow_step = -3 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f3count    = $output['count'];
        $nested_data[] = $f3count;                                               // จัดทำเอกสาร flow = 3 -3

        $tempsql    = $monthsql . ' AND a.flow_step >= 4 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f4count    = $output['count'];
        $nested_data[] = $f4count;                                              // ส่งสัญญา flow = 4

        $tempsql    = $monthsql . ' AND a.flow_step = -3 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f3count2   = $output['count'];
        $nested_data[] = $f3count2;                                             // คืนสัญญา flow = -3

        $tempsql    = $monthsql . ' AND a.flow_step >= 5 AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $f5count    = $output['count'];
        $nested_data[] = $f5count;                                              // รับสัญญา flow = 5

        $tempsql    = $monthsql . ' AND a.flow_step >= 5 AND cl.contract_annual_cost = "0" AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $nocost     = $output['count'];
        $nested_data[] = $nocost;                                               // มีค่าเช่า

        $tempsql    = $monthsql . ' AND a.flow_step >= 5 AND cl.contract_annual_cost != "0" AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $hascost    = $output['count'];
        $nested_data[] = $hascost;                                              // ไม่มีค่าเช่า

        $tempsql    = $monthsql . ' AND a.flow_step >= 5 AND (cl.elec_pay_type = "1" OR cl.elec_pay_type = "2" OR cl.elec_pay_type = "3") AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $haselec    = $output['count'];
        $nested_data[] = $haselec;                                              // มีค่าไฟ

        $tempsql    = $monthsql . ' AND a.flow_step >= 5 AND (cl.elec_pay_type = "4" OR cl.elec_pay_type = "5") AND pl.lot_status != 1';
        $result     = $mysqli->query($tempsql);
        $output     = $result->fetch_array(MYSQLI_ASSOC);
        $noelec     = $output['count'];
        $nested_data[] = $noelec;                                               // ไม่มีค่าไฟ
        
        $json_data[] = $nested_data;
    }

    $mysqli->close();
    $data = array(
        'data'  => $json_data
    );

    echo json_encode($data);
    exit();

?>