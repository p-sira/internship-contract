<?php
    date_default_timezone_set('Asia/Bangkok');
    $today_datetime = new DateTime("now");

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();
    $json_data = array();

    $a_contract_status  = getString('contract_status');
    $a_contract_type    = getString('contract_type');
    $a_elec_pay_type    = getString('elec_pay_type');
    $a_elec_pay_rate    = getString('elec_pay_rate');
    $a_lot_type         = getString('lot_type');

    $json_data = array();
    $count = 0;

    $newsql = 
        'SELECT a.*,   
            cl.log_id, pl.project_name, pl.lot_type, pl.project_location_code,
            cl.contract_number, cl.contract_status, cl.contract_type, 
            cl.contract_start, cl.contract_end, cl.contract_annual_cost,
            cl.elec_pay_type, cl.elec_pay_rate, cl.elec_pay_rate2,
            cl.node_type,
            fl.flow1_finish
         FROM
         (
            SELECT
                m.project_id, m.flow_step,
                m.project_log, m.meter_log,
                m.contract_log_old, m.contract_log_new,
                m.flow_log, m.document_log
            FROM
                tb_main m
            WHERE m.contract_log_old != m.contract_log_new
            UNION
            SELECT
                g.project_id, g.flow_step,
                g.project_log, g.meter_log,
                g.contract_log_old, g.contract_log_new,
                g.flow_log, g.document_log
            FROM
                tb_grand_log g
            WHERE g.contract_log_old != g.contract_log_new
         ) a
        LEFT JOIN tb_contract_log cl 	ON a.contract_log_new = cl.log_id
        LEFT JOIN tb_project_log pl 	ON a.project_log = pl.log_id
        LEFT JOIN tb_flow_log fl 		ON a.flow_log = fl.log_id
        WHERE
            cl.contract_status = 1
            AND (
                cl.node_step IS NULL
                OR cl.node_step != 2
            )
            AND pl.lot_status != 1
            AND (a.flow_step = "2" OR a.flow_step = "-1" OR a.flow_step = "1")';  //ยังค้างเจรจา/ค้างอนุมัติ

    $result_new = $mysqli->query($newsql);
    if($result_new->num_rows > 0){
        while($new = $result_new->fetch_array(MYSQLI_ASSOC)){

            $oldsql = 
               'SELECT 
                  cl.log_id,
                  cl.contract_number, cl.contract_status, cl.contract_type, 
                  cl.contract_start, cl.contract_end, cl.contract_annual_cost,
                  cl.elec_pay_type, cl.elec_pay_rate, cl.elec_pay_rate2,
                  cl.node_type
                FROM tb_contract_log cl
                WHERE cl.log_id = "'.$new['contract_log_old'].'"';

            $result_old = $mysqli->query($oldsql);
            $old = $result_old->fetch_array(MYSQLI_ASSOC);
            $nested_data = array();
            $count++;

            if($new['flow_step']=='1'){    //=1 กำลังเจรจา ยังไม่มีข้อมูลสัญญาใหม่ -> สัญญาใหม่ใส่ช่องว่างไว้
                $nested_data[] = $count;
                $nested_data[] = '';
                $nested_data[] = $new['project_name'];
                $nested_data[] = $new['project_location_code'];
                $nested_data[] = '';
                $nested_data[] = '';
                $nested_data[] = '';
                $nested_data[] = '';
                $nested_data[] = '';
                $nested_data[] = '';

                $nested_data[] = is_numeric($new['lot_type']) ? $a_lot_type[$new['lot_type']] : $new['lot_type'];

                $nested_data[] = $old['contract_number'];
                $nested_data[] = '-';
                $nested_data[] = is_numeric($old['contract_type']) ? $a_contract_type[$old['contract_type']] : $old['contract_type'];
                $nested_data[] = date("d-m-Y", strtotime($old['contract_end']));
                $nested_data[] = $old['contract_annual_cost'];
                $elec_pay_type = is_numeric($old['elec_pay_type']) ? $a_elec_pay_type[$old['elec_pay_type']] : $old['elec_pay_type'];
                $elec_pay_rate = is_numeric($old['elec_pay_rate']) ? $a_elec_pay_rate[$old['elec_pay_rate']] : $old['elec_pay_rate'];
                $nested_data[] = $elec_pay_type.'<br>'.$elec_pay_rate.' '.$old['elec_pay_rate2'];
                $nested_data[] = '-';
                $nested_data[] = date("d-m-Y", strtotime($new['flow1_accept']));

                $nested_data[] = 'รอเจรจา';
            }
            else{
                $nested_data[] = $count;
                $nested_data[] = $new['contract_number'];
                $nested_data[] = $new['project_name'];
                $nested_data[] = $new['project_location_code'];
                $nested_data[] = is_numeric($new['contract_type']) ? $a_contract_type[$new['contract_type']] : $new['contract_type'];
                $nested_data[] = date("d-m-Y", strtotime($new['contract_start']));
                $nested_data[] = date("d-m-Y", strtotime($new['contract_end']));
                $nested_data[] = $new['contract_annual_cost'];

                $elec_pay_type = is_numeric($new['elec_pay_type']) ? $a_elec_pay_type[$new['elec_pay_type']] : $new['elec_pay_type'];
                $elec_pay_rate = is_numeric($new['elec_pay_rate']) ? $a_elec_pay_rate[$new['elec_pay_rate']] : $new['elec_pay_rate'];
                $nested_data[] = $elec_pay_type.'<br>'.$elec_pay_rate.' '.$new['elec_pay_rate2'];
                $nested_data[] = '-';
                $nested_data[] = is_numeric($new['lot_type']) ? $a_lot_type[$new['lot_type']] : $new['lot_type'];

                $nested_data[] = $old['contract_number'];
                $nested_data[] = '-';
                $nested_data[] = is_numeric($old['contract_type']) ? $a_contract_type[$old['contract_type']] : $old['contract_type'];
                $nested_data[] = date("d-m-Y", strtotime($old['contract_end']));
                $nested_data[] = $old['contract_annual_cost'];
                $elec_pay_type = is_numeric($old['elec_pay_type']) ? $a_elec_pay_type[$old['elec_pay_type']] : $old['elec_pay_type'];
                $elec_pay_rate = is_numeric($old['elec_pay_rate']) ? $a_elec_pay_rate[$old['elec_pay_rate']] : $old['elec_pay_rate'];
                $nested_data[] = $elec_pay_type.'<br>'.$elec_pay_rate.' '.$old['elec_pay_rate2'];
                $nested_data[] = '-';

                $nested_data[] = date("d-m-Y", strtotime($new['flow1_finish']));
                $nested_data[] = $new['flow_step'] == '-1' ? 'รอเจรจาใหม่' : 'รออนุมัต';

                $nested_data[] = '<a href="p_information.php?id='.$new['project_id'].'"class="ui compact blue mini button">ดู</a>';
            }

            $json_data[] = $nested_data;
        }
    }
    
    $mysqli->close();
    $data = array(
        'data'  => $json_data
    );
    echo json_encode($data);
    exit();
?>