<?php

    date_default_timezone_set('Asia/Bangkok');
    $today_datetime = new DateTime("now");

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();
    $user = getUserDetail($_SESSION['userID']);

    if($user != 'error'){
      $user_type = $user['user_type'];
      if($user_type == 'NI') 				  $permission = ' AND (pl.lot_type="1" OR pl.lot_type="2")';
      else if($user_type == 'Admin') 	$permission = ' AND (pl.lot_type="3")';
      else if($user_type == 'Sales') 	$permission = ' AND (pl.lot_type="4" OR pl.lot_type="5")';
      else if($user_type == 'God')		$permission = '';
      else if($user_type == 'etc')    $permission = '';
    }
    else{
        $user_type = 'not user';
        $permission = '-';
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $json_data = array();

    if($permission != '-'){
      $sql = 'SELECT 	m.flow_step, m.project_id,
                      pl.project_location_code, pl.project_number, pl.project_name, pl.lot_type,
                      cl.contract_start, cl.contract_end, cl.contract_year, cl.contract_status,
                      cl.elec_pay_type, cl.elec_pay_rate, cl.elec_pay_rate2, 
                      cl.contract_annual_cost, cl.contract_type
              FROM tb_main AS m
              INNER JOIN tb_project_log AS pl
                ON m.project_log = pl.log_id
              INNER JOIN tb_contract_log AS cl
                ON m.contract_log_old = cl.log_id
              WHERE   (cl.contract_status="1") AND (pl.lot_status!="1")'.$permission;      //1=สัญญาต่ออายุอัตโนมัติ

      $result = $mysqli->query($sql);	
      if ($result->num_rows > 0) {

        // เอาค่า string มา map
        $a_contract_status  = getString('contract_status');
        $a_contract_type    = getString('contract_type');
        $a_elec_pay_type    = getString('elec_pay_type');
        $a_elec_pay_rate    = getString('elec_pay_rate');
        $a_lot_type         = getString('lot_type');
        $a_flow_step        = getString('flow_step');
            $a_flow_step[0] = 'ยังไม่รับงาน';

        while ($o = $result->fetch_array(MYSQLI_ASSOC))
        {
          $dateDiff = date_diff( $today_datetime, date_create($o['contract_end']));
          $pos = $dateDiff->format('%R');
          $year = $dateDiff->format('%y');
          $month = $dateDiff->format('%m');

          if($pos == '+'){
            if($year > 0 || ($year == 0 && $month >= 3))  // ถ้าเวลาเกิน 3 เดือน ไม่โชว์
              continue;

            $status = '<p style="color: green;">ยังไม่หมด </p>';
            if($year == 0 && $month == 0)       $dateDiff = $dateDiff->format('%dวัน');
            else if($year == 0 && $month < 3)   $dateDiff = $dateDiff->format('%dวัน / %m เดือน');
          }
          else if($pos == '-'){
            $status = '<p style="color: red;">เกินกำหนด </p>';
            if($year == 0 && $month != 0)       $dateDiff = $dateDiff->format('-%dวัน / %m เดือน');
            else if($year == 0 && $month == 0)  $dateDiff = $dateDiff->format('-%dวัน');
            else $dateDiff = $dateDiff->format('-%dวัน / %m เดือน / %yปี');
          }

          //ปุ่ม ดูรายละเอียด + รับงาน
          $button = '<a href="p_information.php?id='.$o['project_id'].'"class="ui compact blue mini button">ดู</a>';
          if($user_type != 'etc' && $user_type != 'not user'){
              if($o['flow_step'] == 0)          //ปุ่มรับงาน
                  $button .= '<button class="ui compact green mini button" onclick="javascript:acceptWork('.$o['project_id'].');">รับ</button>';
              else if ($o['flow_step'] == 5)    //ปุ่มรับงาน (งานเสร็จแล้ว)
                  $button .= '<button class="ui compact green mini disabled button" onclick="javascript:acceptWork('.$o['project_id'].');">เสร็จ</button>';
              else                              //ปุ่มรับงานแบบกดไม่ได้
                  $button .= '<button class="ui compact green mini disabled button">รับ</button>';
          }  
          
          $o['flow_step'] = $o['flow_step'] < 0 ? -$o['flow_step']:$o['flow_step'];

          $nestedData   = array();
          $nestedData[] = $o['project_location_code'];
          $nestedData[] = $o['project_name'];
          $nestedData[] = $o['project_number'];
          $nestedData[] = is_numeric($o['lot_type']) ? $a_lot_type[$o['lot_type']] : $o['lot_type'];
          $nestedData[] = $o['contract_start'];
          $nestedData[] = $o['contract_end'];
          $nestedData[] = $status;
          $nestedData[] = $dateDiff;
          $nestedData[] = is_numeric($o['contract_status']) ? $a_contract_status[$o['contract_status']] : $o['contract_status'];
          $nestedData[] = is_numeric($o['contract_type']) ? $a_contract_type[$o['contract_type']] : $o['contract_type'];
          $nestedData[] = is_numeric($o['elec_pay_type']) ? $a_elec_pay_type[$o['elec_pay_type']] : $o['elec_pay_type'];
          $temp = is_numeric($o['elec_pay_rate']) ? $a_elec_pay_rate[$o['elec_pay_rate']] : $o['elec_pay_rate'];
          $nestedData[] = $temp.' '.$o['elec_pay_rate2'].'.-';
          $nestedData[] = $o['contract_annual_cost'];
          $nestedData[] = $o['flow_step'] == '0' ? $a_flow_step[$o['flow_step']] : '<p style="color:green;">'.$a_flow_step[$o['flow_step']].'</p>';
          $nestedData[] = $button;
          $json_data[]  = $nestedData;
        }
      }
      $mysqli->close();
    }
    $data = array(
        'data'  => $json_data
    );
    echo json_encode($data);
    exit();
  
  

?>