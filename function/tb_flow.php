<?php

    date_default_timezone_set('Asia/Bangkok');
    $today_datetime = new DateTime("now");

    if(!isset($_SESSION)) { 
        session_start(); 
    } 
    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();
    $user = getUserDetail($_SESSION['userID']);

    if($user != 'error'){
      $user_type = $user['user_type'];
      if($user_type == 'NI') 			$permission = ' AND (pl.lot_type="1" OR pl.lot_type="2")';
      else if($user_type == 'Admin') 	$permission = ' AND (pl.lot_type="3")';
      else if($user_type == 'Sales') 	$permission = ' AND (pl.lot_type="4" OR pl.lot_type="5")';
      else if($user_type == 'God')		$permission = '';
      else if($user_type == 'etc')      $permission = '';
    }
    else{
        $user_type = 'not user';
        $permission = '';
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // เอาค่า WHERE กับ column ที่จะเลือก
    
    $othercol = '';
    if($_GET['step'] == 1){
        $calc_column        =   'flow1_accept';
        $flow_permission    =   ' WHERE (m.flow_step = "1" OR m.flow_step = "-1")';
    }
    else if($_GET['step'] == 2){
        $calc_column        =   'flow2_accept';
        $flow_permission    =   ' WHERE (m.flow_step = "2")';
    }
    else if($_GET['step'] == 3){
        $calc_column        =   'flow3_accept';
        $flow_permission    =   ' WHERE (m.flow_step = "3" OR m.flow_step = "-3")';
    }
    else if($_GET['step'] == 4){
        $calc_column        =   'flow4_accept';
        $flow_permission    =   ' WHERE (m.flow_step = "4")';
    }
    else if($_GET['step'] == 5){
        $calc_column        =   'flow5_accept';
        $flow_permission    =   ' WHERE (m.flow_step = "5")';
        
        $othercol           = ',cl.contract_status, cl.elec_pay_type, 
                                cl.elec_pay_rate, cl.elec_pay_rate2,
                                cl.contract_annual_cost, cl.contract_type';
    }
    //map lot_type
    $a_lot_type         = getString('lot_type');
    $a_elec_pay_type    = getString('elec_pay_type');
    $a_elec_pay_rate    = getString('elec_pay_rate');

    $json_data = array();
    $sql = 'SELECT 	m.flow_step, m.project_id,
                    pl.project_location_code, pl.project_number,
                    pl.log_id, pl.project_name, pl.lot_type,
                    fl.flow1_accept, fl.flow2_accept, fl.flow3_accept,
                    fl.flow4_accept, fl.flow5_accept,
                    fl.flow2_sendmail,
                    dl.doc_type'.$othercol.'
            FROM    tb_main AS m
            LEFT JOIN tb_project_log AS pl
                ON  m.project_log = pl.log_id
            LEFT JOIN tb_contract_log AS cl
                ON  m.contract_log_old = cl.log_id
            LEFT JOIN tb_flow_log AS fl
                ON  m.flow_log = fl.log_id
            LEFT JOIN tb_document_log AS dl
                ON  m.document_log = dl.log_id'.$flow_permission.$permission;
    
    $sql.=' AND (fl.flow2_notapprove != 1 OR fl.flow2_notapprove IS NULL)';

    $result = $mysqli->query($sql);	
    $mysqli->close();

    if ($result->num_rows > 0) {

        $a_doc_type         = getString('doc_type');
        $a_contract_type    = getString('contract_type');
        $a_elec_pay_type    = getString('elec_pay_type');

        while ($output = $result->fetch_array(MYSQLI_ASSOC)) {

            $nestedData   = array();

            if($output['flow_step'] == '1')	{
                $dayRemain = 21;
                $workStatus = 'รอเจรจา';
            }
            else if($output['flow_step'] == '-1')	{
                $dayRemain = 14;
                $workStatus = '<b style="color:orange;">รอเจรจาใหม่</b>';
                $output['flow_step'] = -$output['flow_step'];
            }
            else if($output['flow_step'] == '2')   {
                $dayRemain = 5;
                if($output['flow2_sendmail'] == '1')
                    $workStatus = 'รออนุมัติ';
                else
                    $workStatus = 'รอส่งอีเมล';
            }
            else if($output['flow_step'] == '3')   {
                $dayRemain = 21;
                $workStatus = 'รวบรวมเอกสาร';
            }
            else if($output['flow_step'] == '-3')   {
                $dayRemain = 14;
                $workStatus = '<b style="color:orange;">รวบรวมเอกสารใหม่</b>';
                $output['flow_step'] = -$output['flow_step'];
            }
            else if($output['flow_step'] == '4')   {
                $dayRemain = 2;
                $workStatus = 'ตรวจเอกสาร';
            }
            else if($output['flow_step'] == '5')   {
                $dayRemain = 1;
                $workStatus = 'ส่งต่องาน';
            }

            $dateDiff   = date_diff(date_create($output[$calc_column]), $today_datetime);
            $pos        = $dateDiff->format('%R');
            $dateDiff   = $dayRemain - $dateDiff->format('%R%a');
        
            // 4คอลัมแรก ทุก flow เหมือนกัน
            $nestedData[] = $output['project_location_code'];
            $nestedData[] = $output['project_name'];
            $nestedData[] = $output['project_number'];
            $nestedData[] = $a_lot_type[$output['lot_type']];
            $nestedData[] = $output['flow1_accept'];

            // รับงานวันที่ เหลือเวลา สถานะของงาน (แต่ละ flow ต่างกัน)
            $nestedData[] = $output[$calc_column];
            $nestedData[] = $dateDiff.' วัน';
            if($pos == '-') $nestedData[] = 'b style="color:red;เกินเวลา</b>';
            else            $nestedData[] = $workStatus;

            //
            if($_GET['step']==4) {
                $nestedData[] = is_numeric($output['doc_type']) ? $a_doc_type[$output['doc_type']] : $output['doc_type'];
            }

            if($_GET['step']==5) {
                $nestedData[] = is_numeric($output['contract_type']) ? $a_contract_type[$output['contract_type']] : $output['contract_type'];
                $nestedData[] = is_numeric($output['elec_pay_type']) ? $a_elec_pay_type[$output['elec_pay_type']] : $output['elec_pay_type'];
                $nestedData[] = (is_numeric($output['elec_pay_rate']) ? $a_elec_pay_rate[$output['elec_pay_rate']] : $output['elec_pay_rate']).' '.$output['elec_pay_rate2'].'.-';
                $nestedData[] = $output['contract_annual_cost'] == null ? '0' : $output['contract_annual_cost'];  
            }   

            // ปุ่ม
            $nestedData[] = '<a class="ui compact blue mini button"
                                href="p_information.php?id='.$output['project_id'].'#tab'.$output['flow_step'].'">ดู</a>';
            
            $json_data[]  = $nestedData;
        }
    }
    $data = array(
        'data'  => $json_data
    );
    echo json_encode($data);
    exit();

?>