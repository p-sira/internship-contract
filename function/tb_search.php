<?php

  date_default_timezone_set('Asia/Bangkok');

  if(!isset($_SESSION)) { 
    session_start(); 
  } 
  include_once('function.php');
  include_once('dbconnect.php');
  $mysqli = dbconnect();

  $a_lot_type   = getString('lot_type');
  $a_lot_status = getString('lot_status');

  $searchword   = $_GET['word'];
  $searchfrom   = $_GET['from'];

  if($searchfrom == 1){
    $column = 'project_location_code';
  }
  else if($searchfrom == 2){
    $column = 'project_number';
  }
  else if($searchfrom == 3){
    $column = 'project_name';
  }
  else if($searchfrom == 4){
    $column = 'lot_person';
  }
  else if($searchfrom == 5){
    $column = 'contract_person_name';
  }

  $sql = 'SELECT
            m.flow_step,
            p.project_id,
            p.project_location_code,
            p.project_number,
            p.project_name,
            p.lot_address,
            p.lot_type,
            p.lot_status,
            p.lot_person,
            p.lot_tel,
            c.contract_person_name
          FROM tb_main m
          LEFT JOIN tb_contract_log c ON m.contract_log_old = c.log_id
          LEFT JOIN tb_project_log p ON m.project_log = p.log_id
          WHERE
            '.$column.' LIKE "%'.$searchword.'%"
          ORDER BY
            c.log_id DESC';

  $result = $mysqli->query($sql);

  $json = array();
  if ($result->num_rows > 0) {
    while($output = $result->fetch_array(MYSQLI_ASSOC)){

        $button = '	<a href="p_information.php?id='.$output['project_id'].'"
                     class="ui compact blue mini button">ดู</a>';
        if($output['flow_step'] == 0) 
            $button .= '<button class="ui compact green mini button"
                onclick="javascript:acceptWork('.$output['project_id'].');">รับ</button>';
        else if ($output['flow_step'] == 5)
            $button .= '<button class="ui compact green mini disabled button"
                onclick="javascript:acceptWork('.$output['project_id'].');">เสร็จ</button>';
        else 
            $button .= '<button class="ui compact green mini disabled button">รับ</button>';
        
        $nested = array();

        $nested[] = $output['project_location_code'];
        $nested[] = $output['project_number'];
        $nested[] = $output['project_name'];
        $nested[] = is_numeric($output['lot_type']) ? $a_lot_type[$output['lot_type']] : $output['lot_type'];
        $nested[] = is_numeric($output['lot_status']) ? $a_lot_status[$output['lot_status']] : $output['lot_status'];
        $nested[] = $output['lot_address'];
        $nested[] = $output['lot_person'];
        $nested[] = $output['lot_tel'];
        $nested[] = $output['contract_person_name'];
        $nested[] = $button;
        $json[] = $nested;
    }
  }
  $data = array(
    'data'    =>  $json
  );
  echo json_encode($data);


?>