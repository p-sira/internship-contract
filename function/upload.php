<?php

    date_default_timezone_set('Asia/Bangkok');
    if(!isset($_SESSION)) { 
      session_start(); 
    } 

    include_once('function.php');
    include_once('dbconnect.php');
    $mysqli = dbconnect();

    $project_id = 1;
    $remark = '';

    //สร้างโฟลเดอ ถ้ายังไม่มี
    $target_dir = '../uploads/mailcopy/';
    if (!file_exists($target_dir)) {
        mkdir($target_dir, 0777, true);
    }
    
    //อัพเดทดาต้าเบส (ไปขั้นถัดไป + วันเวลาที่ทำขั้นนี้เสร็จ)	
    $sql = 'SELECT 	m.flow_log, m.contract_log_new,
                    fl.flow2_rollback
            FROM 	tb_main m, tb_flow_log fl
            WHERE 	m.flow_log = fl.log_id AND
                    m.project_id = '.$project_id;
    $result = $mysqli->query($sql);
    $output = $result->fetch_array(MYSQLI_ASSOC);

    if(isset($_FILES['file_email'])) {

        // สร้างชื่อไฟล์จาก lot, no, code, วันที่
        $num_mail = date("Y-m-d");
        $sql = 'SELECT project_lot as lot, project_number as no, project_location_code as code
                FROM  tb_project_log
                WHERE log_id = (SELECT project_log FROM tb_main WHERE project_id ='.$project_id.' )';
        $result = $mysqli->query($sql);
        $output = $result->fetch_array(MYSQLI_ASSOC);

        // ชื่อไฟล์  uploads/mailcopy/mail_T_T68_0145-37_2017-28-06.png
        $ext = explode('.', $_FILES['file_email']['name']);	//เอานามสกุล
        $target_file = $target_dir
                    .'mail_'.$output['lot']
                    .'_'.$output['no']
                    .'_'.$output['code']
                    .'_'.$num_mail
                    .'.'.end($ext);
        echo $target_file;
        //ไฟล์ซ้ำ อัพทับ
        if (move_uploaded_file($_FILES['file_email']['tmp_name'], $target_file)){		//อัพสำเร็จ
          
          $columns    = array('mail_file_path', 'update_datetime');
          $values     = array($target_file, 'CURRENT_TIMESTAMP()');
          $condition  = ' WHERE log_id = (SELECT project_log FROM tb_main WHERE project_id ='.$project_id.' )';
          update('tb_document_log', $columns, $values, $condition);

        }
        else {
         $mysqli->close();
         exit;
        }
    }
    $mysqli->close();
    exit;

?>