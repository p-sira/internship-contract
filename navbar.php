<?php 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
    $_SESSION['userID'] = 7;

    $usertype = 'not user';
    $permission = array(1,0,0,0,0,0);
    $navmenu 		= array('ใกล้หมดสัญญา', 'เจรจา', 'อนุมัติ', 'เอกสารสัญญา',
                        'ตรวจรับสัญญา', 'ส่งสัญญาตั้งเบิก');
    $navlink 		= array('p_3month.php', 'p_flow1.php', 'p_flow2.php', 
                        'p_flow3.php' , 'p_flow4.php', 'p_flow5.php');
    $navlength 	= count($permission);

    if(isset($_SESSION['userID'])) { 
      $userID = $_SESSION['userID'];

      include_once('function/dbconnect.php');
      $mysqli = dbconnect();

      $sql = 'SELECT user_nav, special_permission, user_type FROM tb_user WHERE user_id='.$userID;
      $result = $mysqli->query($sql);	
      if($result->num_rows > 0){
        $output = $result->fetch_array(MYSQLI_ASSOC);
        $usernav = $output['user_nav'];
        $usertype = $output['user_type'];

        if($output['user_nav'] == '0')        // คนทั่วไป ดูได้แค่รายละเอียด
          $permission = array(1,0,0,0,0,0);

        else if($output['user_nav'] == '1')  //  ทีมต่อสัญญา
          $permission = array(1,1,1,1,0,0);

        else if($output['user_nav'] == '2' 
             || $output['user_nav'] == '3') // Headแต่ละฝ่าย + Admin
          $permission = array(1,1,1,1,1,1);

        if( $output['special_permission'] != null) {
          $special_permission = explode( ',', $output['special_permission'] );
          $length = count($special_permission);
          for ($i=0; $i < $length ; $i++) {
            $permission[$special_permission[$i]] = 1;
          }
        }
      }
      $mysqli->close();
    }
  
?>
  <div class="ui main menu">
    <div class="header item" class="header item">
      <i class="book icon"></i>
      งานสัญญา
    </div>
    <?php
      for ($i=0; $i < $navlength; $i++) { 
        if($permission[$i]) {
          if($i==$page)
            echo '<a id="nav'.$i.'" href="'.$navlink[$i].'" class="item active"> '.$navmenu[$i].'</a>';
          else
            echo '<a id="nav'.$i.'" href="'.$navlink[$i].'" class="item"> '.$navmenu[$i].'</a>';
        }
      }
    ?>

    <div class="right menu">
      <a href="p_search.php" class="item <?php if($page==8) echo 'active'?>">
        <i class="search icon"></i> สืบค้น 
      </a>
      <div class="ui simple dropdown item "> รายงาน <i class="dropdown icon"></i>
        <div class="menu">
          <a href="report1.php" class="item">1 - สรุปสัญญาที่จะหมดในเดือนต่างๆ</a>
          <a href="report2.php" class="item">2 - สรุปสัญญาที่ต่อเสร็จ</a>
          <a href="report3.php" class="item">3 - สรุปงานต่อสัญญา (ล่วงหน้า 3 เดือน)</a>
          <a href="report4.php" class="item">4 - โครงการที่อนุมัตต่อสัญญา</a>
          <a href="report5.php" class="item">5 - โครงการที่ไม่อนุมัตต่อสัญญา</a>
          <a href="report6.php" class="item">6 - งานต่อสัญญาคงค้าง</a>
        </div>
      </div>
    <?php
      if($usertype == 'God'){
        echo '<a href="admin.php" class="item ';
        echo $page==9 ? 'active' : '';
        echo ' "><i class="configure icon"></i> จัดการข้อมูล </a>';
      }
    ?>
    </div>
      <div class="ui label item"> ทีม <?=$usertype ?> </div>
    </div>
    
    </div>

  </div>
  <link rel="shortcut icon" href="favicon.ico">
