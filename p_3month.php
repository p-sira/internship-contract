<!DOCTYPE html>

  <?php 
    $page = 0; 
    if(!isset($_SESSION)) { 
        session_start(); 
    }
  ?>

<html lang="en">
  <head>
      <title> ข้อมูลโครงการ </title>
      <?php include 'config/header.php' ?>
  </head>
  <body>
    <?php include 'navbar.php' ?>
    <div style="padding: 14px; padding-top: 0px">
        
      <div class="ui segments">

        <div class="ui secondary segment">
            <div class="ui header"> ข้อมูลโครงการ </div>			
        </div>
        <div class="ui segment">
            <!-- TABLE HERE -->
            <table id="dt_3month" class="fixed cell-border row-border hover order-column nowrap"
                                  cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>Loc. code</th>
                  <th>โครงการ</th>
                  <th>no.</th>
                  <th>ประเภทพื้นที่</th>
                  <th>สัญญาเริ่ม</th>
                  <th>สัญญาหมด</th>
                  <th>อายุสัญญา</th>
                  <th>คงเหลือ</th>
                  <th>ลักษณะสัญญา</th>
                  <th>ประเภท</th>
                  <th>ค่าไฟ</th>
                  <th>อัตราค่าไฟ</th>
                  <th>ค่าเช่า/ปี</th>
                  <th>ขั้นตอน</th>
                  <th>ตัวเลือก</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Loc. code</th>
                  <th>โครงการ</th>
                  <th>no.</th>
                  <th>ประเภทพื้นที่</th>
                  <th>สัญญาเริ่ม</th>
                  <th>สัญญาหมด</th>
                  <th>อายุสัญญา</th>
                  <th>คงเหลือ</th>
                  <th>ลักษณะสัญญา</th>
                  <th>ประเภท</th>
                  <th>ค่าไฟ</th>
                  <th>อัตราค่าไฟ</th>
                  <th>ค่าเช่า/ปี</th>
                  <th>ขั้นตอน</th>
                  <th>ตัวเลือก</th>
                </tr>
              </tfoot>
            </table>

          </div>
      </div>

    </div>
  </body>

  <!-- JAVASCRIPT HERE -->
  <?php include 'config/footer.php' ?>
  <script type="text/javascript" src="acceptwork.js"></script>
  <script>

    $(document).ready(function () {
      $('#dt_3month').dataTable({

        "iDisplayLength": 25,
        "select": true,
        "scrollX": true,
        "fixedColumns": {
            "leftColumns": 2,
            "rightColumns": 1
        },
        "order": [[ 5, "desc" ]],
        "columnDefs": [
            { className: "dt-body-center", "targets": [0,2,3,5,6,7,9,11,12,13,14] }
        ],
                "ajax": {
            "url": 'function/tb_3month.php'
        }           
    });
            
      $('div.dataTables_filter').addClass('ui input');
      $('div.dataTables_filter input').addClass('sh');
      $('div.dataTables_length select').addClass('ui compact dropdown');
      $('div.dataTables_length select').dropdown();
    });

  </script>
</html>