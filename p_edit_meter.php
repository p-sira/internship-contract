<!DOCTYPE html>

  <?php 
    $page = 7; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
  ?>
  
<html lang="en">
  <head>
    <title> แก้ไขข้อมูลมิเตอร์ </title>
    <?php include 'config/header.php' ?>
  </head>
  <body>
    <?php include 'navbar.php' ?>
      <div class="ui text container" >
        <div class="ui segments">

          <div class="ui secondary segment">
            <div class="ui header"> แก้ไขข้อมูลมิเตอร์ </div>
          </div>
          <div class="ui segment">
            <form class="ui form" method="post" id="form_editmeter">
              <?php include 'form/form_meter.php' ?>
              <br>
              <div class="ui equal width grid">
                <div class="column"></div>
                <div class="column">
                    <button class="fluid large blue ui button pop" id="btn_editmeter">
                    บันทึก</button></div>
                <div class="column"></div>
              </div><br>
            </form>
          </div>

        </div> <!--segment ใหญ่-->
        
      </div> <!--container-->
  </body>
  
  <?php include 'config/footer.php' ?>
  <script>
    var project_id = '';
        project_id = '<?= $_GET["id"] ?>';

    $(document).ready(function () {

        $.post('function/loadmeter.php', {project_id: project_id}, function(output) {
          //console.log(output);
          $("[name=project_name]").val(output.project_name);
          $("[name=project_location_code]").val(output.project_location_code);
          
          $("[name=meter_plan]").val(output.meter_plan);
          $("[name=meter_RP]").val(output.meter_RP);
          $("[name=meter_status]").dropdown('set selected',output.meter_status);
          $("[name=meter_number]").val(output.meter_number);
          $("[name=meter_contract_number]").val(output.meter_contract_number);
          $("[name=meter_doc1]").val(output.meter_doc1);
          $("[name=meter_doc2]").val(output.meter_doc2);
          $("[name=meter_send]").val(output.meter_send);
          $("[name=meter_check]").val(output.meter_check);
          $("[name=meter_start_date]").val(output.meter_start_date);
          $("[name=meter_something_number]").val(output.meter_something_number);
          $("[name=meter_who_pay]").val(output.meter_who_pay);
          $("[name=meter_remark]").val(output.meter_remark);
          
        },'json');

      $('#form_editmeter').form({
        inline: true,
        onSuccess: function(event, fields) {
          event.preventDefault();
          if(confirm('บันทึกข้อมูล?')){
            var data = $('#form_editmeter').serializeArray();
            data.push({name: 'project_id', value: project_id});
            console.log(data);
            $.post('function/editmeter.php', data, function() {
            		location.reload();
            });
          }
          return false;
        }
      });


    });
  </script>
</html>
