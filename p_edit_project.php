<!DOCTYPE html>

  <?php 
    $page = 7; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
  ?>
  
<html lang="en">
  <head>
    <title> แก้ไขข้อมูลโครงการ </title>
    <?php include 'config/header.php' ?>
  </head>
  <body>
    <?php include 'navbar.php' ?>
      <div class="ui text container" >
        <div class="ui segments">

          <div class="ui secondary segment">
            <div class="ui header"> แก้ไขข้อมูลโครงการ </div>
          </div>

          <div class="ui segment">
            <form class="ui form" method="post" id="form_editproject">
              <?php include 'form/form_project.php' ?>
              <br>
              <div class="ui equal width grid">
                <div class="column"></div>
                <div class="column">
                    <button class="fluid large blue ui button pop" id="btn_editproject">
                    บันทึก</button></div>
                <div class="column"></div>
              </div><br>
            </form>
          </div>

        </div> <!--segment ใหญ่-->
        
      </div> <!--container-->
  </body>
  
  <?php include 'config/footer.php' ?>
  <script>
    var project_id = '';
        project_id = '<?= $_GET["id"] ?>';

    $(document).ready(function () {

      $.post('function/getstring.php?column=lot_status+lot_type', function(out) {	//ค่า dropdown 
        var display = '<option value="">เลือกสถานะ</option>';
        for (var i = 0; i < out.length; i++) {
          if(out[i].lot_status == null)
            break;
          display += '<option value="'+out[i].id+'">'+out[i].lot_status+'</option>';
        }
        $('#lot_status').html(display);

        var display = '<option value="">เลือกประเภทพื้นที่</option>';
        for (var i = 0; i < out.length; i++) {
          if(out[i].lot_type == null)
            break;
          display += '<option value="'+out[i].id+'">'+out[i].lot_type+'</option>';
        }
        $('#lot_type').html(display);

        $.post('function/loadproject.php', {project_id: project_id}, 
          function(output) {
            // console.log(output);
            $("[name=project_name]").val(output.project_name);
            $("[name=project_lot]").val(output.project_lot);
            $("[name=project_number]").val(output.project_number);
            $("[name=project_location_code]").val(output.project_location_code);
            $("[name=lot_person]").val(output.lot_person);
            $("[name=lot_tel]").val(output.lot_tel);
            $("[name=lot_address]").val(output.lot_address);
            $("[name=lot_district]").val(output.lot_district);
            //$('#lot_district').dropdown('set selected', output.lot_district);
            $('#lot_status').dropdown('set selected', output.lot_status);
            $('#lot_type').dropdown('set selected', output.lot_type);
            $("[name=date_start]").val(output.date_start);
            $("[name=date_finish]").val(output.date_finish);
            $("[name=date_on_service]").val(output.date_on_service);
            
          },'json'
        );

      },'json');

      $('#form_editproject').form({
        inline: true,
        fields: {		// validate ->
          project_name: {
            identifier: 'project_name',
            rules: [{
                type   : 'empty',       prompt : 'กรอกชื่อโครงการ'
            }]
          },
          project_lot: {
            identifier: 'project_lot',
            rules: [{
                type   : 'empty',       prompt : 'กรอก Lot โครงการให้ถูกต้อง'
            }]
          },
          project_number: {
            identifier: 'project_number',
            rules: [{
                type   : 'empty',       prompt : 'กรอกเลขโครงการให้ถูกต้อง'
            }]
          },
          project_location_code: {
            identifier: 'project_location_code',
            rules: [{
                type   : 'empty',       prompt : 'กรอก location code ให้ถูกต้อง'
            }]
          },
          lot_district: {
            identifier: 'lot_district',
            rules: [{
                type   : 'empty',       prompt : 'เลือกเขตโครงการให้ถูกต้อง'
            }]
          },
          lot_status: {
            identifier: 'lot_status',
            rules: [{
                type   : 'empty',       prompt : 'เลือกสถานะพื้นที่ให้ถูกต้อง'
            }]
          },
          lot_type: {
            identifier: 'lot_type',
            rules: [{
                type   : 'empty',       prompt : 'เลือกประเภทพื้นที่ให้ถูกต้อง'
            }]
          }
        },
        onSuccess: function(event, fields) {
          event.preventDefault();
          if(confirm('บันทึกข้อมูล?')){
            var data = $('#form_editproject').serializeArray();
            data.push({name: 'project_id', value: project_id});
            //console.log(data);
            $.post('function/editproject.php', data, function() {
            		location.reload();
            }).fail(function(xhr, status, error) {
                console.log(xhr.responseText);
            });
          }
          return false;
        }
      });


    });
  </script>
</html>
