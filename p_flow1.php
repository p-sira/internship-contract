<!DOCTYPE html>

  <?php 
    $page = 1; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
  ?>

<html lang="en">
  <head>
    <title> รอเจรจา </title>
    <?php include 'config/header.php' ?>
  </head>
  <body>
    <?php include 'navbar.php' ?>
    <div style="padding: 14px; padding-top: 0px">		

      <div class="ui segments">

        <div class="ui secondary segment">
          <div class="ui header"> โครงการที่รอผลเจรจา </div>			
        </div>
        <div class="ui segment">
          <!-- TABLE HERE -->
          <table id="dt_flow1" class="cell-border row-border hover order-column nowrap"
                  cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Loc. code</th>
                <th>โครงการ</th>
                <th>no.</th>
                <th>ประเภทพื้นที่</th>
                <th>รับงานวันที่</th>
                <th>เริ่มขั้นเจรจา</th>
                <th>เหลือเวลา(21)</th>
                <th>สถานะ</th>
                <th></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Loc. code</th>
                <th>โครงการ</th>
                <th>no.</th>
                <th>ประเภทพื้นที่</th>
                <th>รับงานวันที่</th>
                <th>เริ่มขั้นเจรจา</th>
                <th>เหลือเวลา(21)</th>
                <th>สถานะ</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
        
    </div>
  </body>

  <?php include 'config/footer.php' ?>
  <script>
    $(document).ready(function () {
      $('#dt_flow1').dataTable({
        
        "select": true,
        "scrollX": true,
        "fixedColumns": {
            "leftColumns": 2,
            "rightColumns": 1
        },
        "columnDefs": [
            { className: "dt-body-center", "targets": [0,2,3,4,5,6,7] }
        ],
        "ajax": {
            "url": 'function/tb_flow.php?step=1'
        },
        language: {
            emptyTable: "No data available in table", // 
            loadingRecords: "Please wait .. ", // default Loading...
            zeroRecords: "No matching records found"
        }
      });

      $('div.dataTables_filter').addClass('ui input');
      $('div.dataTables_filter input').addClass('sh');
      $('div.dataTables_length select').addClass('ui compact dropdown');
      $('div.dataTables_length select').dropdown();

    });

  </script>
</html>