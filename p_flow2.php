<!DOCTYPE html>

	<?php 
		$page = 2; 
		if(!isset($_SESSION)) { 
			session_start(); 
		}
	?>

<html lang="en">
	<head>
		<title> รออนุมัติ </title>
		<?php include 'config/header.php' ?>
	</head>
	<body>
		<?php include 'navbar.php' ?>
		<div style="padding: 14px; padding-top: 0px">
				
				<div class="ui segments">

					<div class="ui secondary segment">
						<div class="ui header"> โครงการที่รออนุมัติ </div>			
					</div>
					<div class="ui segment">
						<!-- TABLE HERE -->
						<table id="dt_project_flow2" class="cell-border row-border hover order-column nowrap"
									 cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Loc. code</th>
									<th>โครงการ</th>
									<th>no.</th>
									<th>ประเภทพื้นที่</th>
									<th>รับงานวันที่</th>
									<th>เริ่มขั้นอนุมัติ</th>
									<th>เหลือเวลา (5)</th>
									<th>สถานะ</th>
									<th></th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Loc. code</th>
									<th>โครงการ</th>
									<th>no.</th>
									<th>ประเภทพื้นที่</th>
									<th>รับงานวันที่</th>
									<th>เริ่มขั้นอนุมัติ</th>
									<th>เหลือเวลา (5)</th>
									<th>สถานะ</th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

        </div>
	</body>

	<?php include 'config/footer.php' ?>
	  <script>
    $(document).ready(function () {
			$('#dt_project_flow2').dataTable({
			
					"select": true,
					"scrollX": true,
					"fixedColumns": {
							"leftColumns": 2,
							"rightColumns": 1
					},
					"columnDefs": [
							{ className: "dt-body-center", "targets": [0,2,3,4,5,6,7] }
					],
					"ajax": {
							"url": 'function/tb_flow.php?step=2'
					}
    	});

      $('div.dataTables_filter').addClass('ui input');
      $('div.dataTables_filter input').addClass('sh');
      $('div.dataTables_length select').addClass('ui compact dropdown');
      $('div.dataTables_length select').dropdown();

    });

  </script>
</html>