<!DOCTYPE html>

  <?php 
    $page = 5; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
  ?>

<html lang="en">
  <head>
    <title> ต่อสัญญาเสร็จ </title>
    <?php include 'config/header.php' ?>
  </head>
  <body>
    <?php include 'navbar.php' ?>
    <div style="padding: 14px; padding-top: 0px">		
      <div class="ui segments">

        <div class="ui secondary segment">
          <div class="ui header"> โครงการที่ต่อสัญญาเสร็จ </div>			
        </div>
        <div class="ui segment">
          <table id="dt_project_flow5" class="cell-border row-border hover order-column nowrap" 
                 cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Loc. code</th>
                <th>โครงการ</th>
                <th>no.</th>
                <th>ประเภทพื้นที่</th>
                <th>รับงานวันที่</th>
                <th>เริ่มขั้นส่งต่อเอกสาร</th>
                <th>เหลือเวลา(1)</th>
                <th>สถานะ</th>
                <th>ประเภท</th>
                <th>ค่าไฟ</th>
                <th>อัตราค่าไฟ</th>
                <th>ค่าเช่า/ปี</th>
                <th></th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>Loc. code</th>
                <th>โครงการ</th>
                <th>no.</th>
                <th>ประเภทพื้นที่</th>
                <th>รับงานวันที่</th>
                <th>เริ่มขั้นส่งต่อเอกสาร</th>
                <th>เหลือเวลา(1)</th>
                <th>สถานะ</th>
                <th>ประเภท</th>
                <th>ค่าไฟ</th>
                <th>อัตราค่าไฟ</th>
                <th>ค่าเช่า/ปี</th>
                <th></th>
              </tr>
            </tfoot>
          </table>
        </div>
        
      </div>
    </div>
  </body>

  <?php include 'config/footer.php' ?>
  <script>
    $(document).ready(function () {
      $('#dt_project_flow5').dataTable({
    
          "select": true,
          "scrollX": true,
          "fixedColumns": {
            "leftColumns": 2,
            "rightColumns": 1
          },
          "columnDefs": [
            { className: "dt-body-center", "targets": [0,2,3,4,5,6,7,8,9,10,11] }
          ],
          "ajax": {
            "url": 'function/tb_flow.php?step=5'
          }
      });

      $('div.dataTables_filter').addClass('ui input');
      $('div.dataTables_filter input').addClass('sh');
      $('div.dataTables_length select').addClass('ui compact dropdown');
      $('div.dataTables_length select').dropdown();

    });

  </script>
</html>
