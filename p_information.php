<!DOCTYPE html>

<?php 
    $page = 7; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
    $usertype = 'not user';
    $permission = array(1,0,0,0,0,0);
    $navmenu 		= array('รายชื่อโครงการ', 'เจรจา', 'อนุมัติ', 'เอกสารสัญญา',
                        'ตรวจรับสัญญา', 'ส่งสัญญาตั้งเบิก');
    $navlength 	= count($permission);

    if(isset($_SESSION['userID'])) { 
      $userID = $_SESSION['userID'];
      include_once('function/dbconnect.php');
      $mysqli = dbconnect();

      $sql = 'SELECT user_nav, special_permission, user_type FROM tb_user WHERE user_id='.$userID;
      $result = $mysqli->query($sql);	
      if($result->num_rows > 0){
        $output = $result->fetch_array(MYSQLI_ASSOC);
        $usernav = $output['user_nav'];
        $usertype = $output['user_type'];

        if($output['user_nav'] == '0')        // คนทั่วไป ดูได้แค่รายละเอียด
          $permission = array(1,0,0,0,0,0);

        else if($output['user_nav'] == '1')  //  ทีมต่อสัญญา
          $permission = array(1,1,1,1,0,0);

        else if($output['user_nav'] == '2' 
             || $output['user_nav'] == '3') // Headแต่ละฝ่าย + Admin
          $permission = array(1,1,1,1,1,1);

        if( $output['special_permission'] != null) {
          $special_permission = explode( ',', $output['special_permission'] );
          $length = count($special_permission);
          for ($i=0; $i < $length ; $i++) {
            $permission[$special_permission[$i]] = 1;
          }
        }
      }
      $mysqli->close();
    }
?>

<html lang="en">
  <head>
    <title id="title"> รายละเอียดโครงการ </title>
    <?php include 'config/header.php' ?>
  </head>

  <style>
    .my-header {
      text-align: center;
      font-weight: bold;
      font-size: 150%;
    }
    .sh {
      padding-top: 8px			  !important;
      padding-bottom: 8px	    !important;
    }
  </style>

  <body>
    <?php include 'navbar.php' ?>
    <div class="ui container">

      <div class="ui internally celled grid segment">
      
        <div class="four wide column">
          <div class="ui fluid secondary vertical menu">
            <a class="item active" data-tab="tab0" id="tab0"> รายละเอียด </a>
            <div class="ui divider"></div>
            <?php
              for ($i=1; $i < $navlength; $i++) { 
                if($permission[$i]) {
                  echo '<a class="item" data-tab="tab'.$i.'" id="tab'.$i.'"> '.$navmenu[$i].'</a>';
                }
              }
            ?>
          </div>
        </div>

        <div class="twelve wide column">
          <div>
            <p class="my-header" id="head_project_name"> ชื่อโครงการ </p>
          </div><br>
          <div class="ui tab active" data-tab="tab0">	
            <?php include 'tab/tab0.html' ?>
          </div>
          <div class="ui tab" data-tab="tab1">			
            <?php include 'tab/tab1.html' ?>
          </div>
          <div class="ui tab" data-tab="tab2">
            <?php include 'tab/tab2.html' ?>
          </div>
          <div class="ui tab" data-tab="tab3">
            <?php include 'tab/tab3.html' ?>
          </div>
          <div class="ui tab" data-tab="tab4">
            <?php include 'tab/tab4.html' ?>
          </div>
          <div class="ui tab" data-tab="tab5">
            <?php include 'tab/tab5.html' ?>
          </div>
        </div>
      
      </div> <!--end of celled grid segment-->
    </div> <!--end of container-->
  </body>


  <?php include 'config/footer.php' ?>
  <script type="text/javascript" src="tab/loadData.js"></script>
  <script type="text/javascript" src="tab/setData.js"></script>
  <script type="text/javascript" src="tab/setTab1.js"></script>
  <script type="text/javascript" src="tab/setTab2.js"></script>
  <script type="text/javascript" src="tab/setTab3.js"></script>
  <script type="text/javascript" src="tab/setTab4.js"></script>
  <script type="text/javascript" src="tab/setTab5.js"></script>
  <script>
    moment.locale('th');

    var project_id = '';
        project_id = '<?= $_GET["id"] ?>';			// id โปรเจคได้จาก url
    var menu_array = [ '', 'เจรจา', 'อนุมัติ', 'เอกสารสัญญา', 'ตรวจรับสัญญา', 'ส่งสัญญาตั้งเบิก' ];
    var docs;                                   //docs = ชื่อเอกสารที่ต้องรวบรวมขั้น 3+4
    var data;                                   //ข้อมูลของโปรเจคปัจจุบัน
  
  $(document).ready(function () {
    if(project_id != '') {

      $('.menu .item').tab({                    //ให้ tab จำหน้าเวลา reload (
        history: true,                          //ใช้ร่วมกับ js -> migrate + jqAddress ในอินคลูด)
        historyType: 'hash'
      });


      var columns = 'doc_type+doc_require+contract_type+contract_status+contract_pay_cost+';
          columns+= 'elec_pay_type+elec_pay_rate+elec_fee+month+';
          columns+= 'node_type+node_type2+node_step+lot_type+lot_status';
      $.post('function/getstring.php?forarray&column='+columns,
        function(string){

          $.post('function/loaddata.php', {project_id: project_id},
            function(output){
              data = output;
              console.log(output);
              
              loadDropdown(string, output.oldlog);              // tab/loadData.js ตั้งค่าdropdown จากstring + ค่าdropdownสัญญาเก่า
              loadTab(output.m['flow_step']);                   // tab/loadData.js

              setData(data, string);                            // tab/setData.js พวกค่าในฟอร์มต่างๆ

              setTab1(project_id);                              // validate form + dropdown old contract
              setTab2(project_id , output.p, output.co, output.cn, string); //ปุ่มกด + ข้อมูลอีเมล
              setTab3(project_id);
              setTab4(project_id);
              setTab5(project_id);

            },'json'
          );

        },'json'
      );
      
    }
  });
  </script>
</html>