<!DOCTYPE html>

	<?php 
		$page = -1; 
		if(!isset($_SESSION)) { 
			session_start(); 
		}
	?>
	
<html lang="en">
	<head>
		<title> สืบค้นสัญญา </title>
		<?php include 'config/header.php' ?>
	</head>
	<body>
		<?php include 'navbar.php' ?>
    <div class="ui container">
		
			
			<div class="ui segments">
				<div class="ui secondary segment">
							<h3>สืบค้นสัญญา</h3>
				</div>	
				<div class="ui segment">
					<div class="ui centered grid">
						<div class="twelve wide column">

							<div class="ui fluid action input">
								<input type="text" placeholder="คำค้นหา..." id="searchword">
								<select class="ui selection dropdown" id="dd_searchfrom">
									<option value="1">จาก Location Code</option>
									<option value="2">จาก Lot no.</option>
									<option value="3">จาก ชื่อโครงการ</option>
									<option value="4">จาก ชื่อผู้ติดต่อ</option>
									<option value="5">จาก ชื่อผู้ทำสัญญา</option>
								</select>
								<div class="ui right labeled icon button" id="btn_search">
									<i class="right arrow icon"></i>ค้นหา
								</div>
							</div>
						</div>
					</div>
				</div>

				
		</div>
		
		<table id="dt_searchproject" class="cell-border row-border hover order-column nowrap"
				cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Loc. code</th>
					<th>no.</th>
					<th>โครงการ</th>
					<th>ประเภท</th>
					<th>สถานะ</th>
					<th>ที่อยู่</th>
					<th>ชื่อผู้ติดต่อ</th>
					<th>เบอร์โทรผู้ติดต่อ</th>
					<th>ชื่อผู้ทำสัญญา</th>
					<th>ตัวเลือก</th>
				</tr>
			</thead>
		</table>
						

	</body>
	
	<?php include 'config/footer.php' ?>
	<script type="text/javascript" src="acceptwork.js"></script>
	<script>

		function callTable(word, from){

			$('#dt_searchproject').dataTable({
				"iDisplayLength": 25,
				"select": true,
				"scrollX": true,
				"fixedColumns": {
						"leftColumns": 3,
						"rightColumns": 1
				},
				"bDestroy": true,
				"bSort" : false,
				"ajax": {
						"url": 'function/tb_search.php?word='+word+'&from='+from
				}
			});
				
			$('div.dataTables_filter').addClass('ui input');
      $('div.dataTables_filter input').addClass('sh');
      $('div.dataTables_length select').addClass('ui compact dropdown');
			$('div.dataTables_length select').dropdown();
    }

    $(document).ready(function () {

			$('#dd_searchfrom').dropdown();

			$('#btn_search').click(function() {
					var searchword = $('#searchword').val();
					var searchfrom = $('#dd_searchfrom').val();
				
					callTable(searchword, searchfrom);
					$('div.dataTables_filter').addClass('ui input');
      		$('div.dataTables_filter input').addClass('sh');
      		$('div.dataTables_length select').addClass('ui dropdown');
					$('div.dataTables_length select').dropdown();
			});


			$('#dt_searchproject').dataTable({
          "select": true,
          "scrollX": true,
          "fixedColumns": {
              "leftColumns": 3,
            	"rightColumns": 1
          },
          "bDestroy": true
			});
			
			$('div.dataTables_filter').addClass('ui input');
      $('div.dataTables_filter input').addClass('sh');
      $('div.dataTables_length select').addClass('ui compact dropdown');
			$('div.dataTables_length select').dropdown();
			
    });

  </script>
</html>
