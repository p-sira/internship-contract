<!DOCTYPE html>
<?php 
  $page = -1; 
  if(!isset($_SESSION)) { 
    session_start(); 
  }
?>
	
<html lang="en">
	<head>
		<title> Report 1 </title>
		<?php include 'config/header.php' ?>
	</head>
    <style>
      tr,td,th {
        padding: 5px    !important;
      }
    </style>
    <body>
        
    <?php include 'navbar.php' ?>
		<div style="padding: 14px; padding-top: 0px">		

    <div class="ui segments">	
      <div class="ui secondary segment">
        <h3>
          Report 1 : จำนวนสัญญาที่จะหมดภายในเดือนต่างๆ
        </h3>
      </div>
      <div class="ui segment">
        
        <div class="ui centered grid">
          <div class="five wide column">
            <b>เลือกปี</b>
            &nbsp;&nbsp;&nbsp;
            <select class="ui dropdown" name="year" id="year">
              <option value="">เลือกปี</option>
            </select>
            &nbsp;&nbsp;&nbsp;
            <button class="ui right labeled icon button"  id="btn_report1">
              <i class="right arrow icon"></i>
              ตกลง
            </button>
          </div>
        </div>

      </div>
    </div>

    <table class="ui selectable celled structured fixed table"
           cellspacing="0" width="100%" id="table_report1">
      <thead>
        <tr class="center aligned">
          <th colspan="1">สัญญา</th>
          <th colspan="4" >หน่วยงาน/ชุมชน</th>
          <th colspan="4">คอนโด/อพาร์ตเม้นต์</th>
          <th colspan="4">ที่พัก/หมู่บ้าน</th>
          <th colspan="1">รวม</th>
        </tr>
        <tr class="center aligned">
          <th>ครบอายุ</th>
          <th>DSLAM</th>
          <th>ME</th>
          <th>OLT</th>
          <th>อื่นๆ</th>
          <th>DSLAM</th>
          <th>KC</th>
          <th>FT</th>
          <th>อื่นๆ</th>
          <th>DSLAM</th>
          <th>ME</th>
          <th>OLT</th>
          <th>อื่นๆ</th>
          <th>สัญญา</th>
        </tr>
      </thead>
    </table>

    </div>

    </body>
        
	<?php include 'config/footer.php' ?>
  <script>
    function callTable(selectedYear){

      $('#table_report1').dataTable({
        "select": true,
        "columnDefs": [
            {  className: "dt-body-center",            "targets": [1,2,3,4,5,6,7,8,9,10,11,12,13] }
        ],
        "bDestroy": true,
        "bSort" : false,
        "bInfo": false,
        "paging": false,
        "searching": false,
        "ajax": {
            "url": 'function/r1.php?year='+selectedYear,
            "type": "POST"
        }
      });

    }

    $(document).ready(function () {

      var cur_date  = new Date();
      var cur_month = cur_date.getMonth();
      var cur_year  = cur_date.getFullYear();
      var start_year = 2016;

      display = '<option value="">เลือกปี</option>';
      for (year = cur_year+5; year >= start_year; year--) {
        display += '<option value="'+year+'">'+year+'</option>';
      }
      $('#year').html(display);
      $('#year').dropdown('set selected', cur_year);

      callTable(cur_year);

      $('#btn_report1').click(function() {
        var selectedYear  = $('#year').val();
        callTable(selectedYear);
      });

    }); //jq DocReady
  </script>

</html>
