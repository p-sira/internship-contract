<!DOCTYPE html>
<?php 
    $page = -1; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
?>
	
<html lang="en">
	<head>
		<title> Report 3 </title>
		<?php include 'config/header.php' ?>
	</head>
    <style>
      tr,td,th {
        padding-left:   3px    !important;
        padding-right:  3px    !important;
        padding-top:    5px    !important;
        padding-bottom: 5px    !important;
        text-align: center;
      }
      .b-red {
        background-color: #FF8A80;
      }
      .b-orange {
        background-color: #FFD180;
      }
      .b-yellow {
        background-color: #FFFF8D;
      }
      .b-lime {
        background-color: #F4FF81;
      }
      .b-green {
        background-color: #CCFF90;
      }
      .b-indigo {
        background-color: #7986CB;
        color: white;
      }
      
    </style>
    <body>
        
    <?php include 'navbar.php' ?>
		<div style="padding: 14px; padding-top: 0px">		

    <div class="ui segments">	
      <div class="ui secondary segment">
        <h3>
          Report 3 : สรุปงานต่อสัญญา (ล่วงหน้า 3 เดือน)
        </h3>
      </div>
      <div class="ui segment">
        
        <div class="ui centered grid">
          <div class="five wide column">
            <b>เลือกปี</b>
            &nbsp;&nbsp;&nbsp;
            <select class="ui search selection dropdown" name="year" id="year">
              <option value="">เลือกปี</option>
            </select>
            &nbsp;&nbsp;&nbsp;
            <button class="ui right labeled icon button"  id="btn_report3">
              <i class="right arrow icon"></i>
              ตกลง
            </button>
          </div>
        </div>

      </div>
    </div>

    <table class="fixed cell-border row-border hover order-column nowrap"
           cellspacing="0" width="100%" id="table_report3">
      <thead>
        <tr class="center aligned">
          <th colspan="7">Contract Renew Progress</th>
          <th colspan="4" class="b-red">1. เจรจาผลตอบแทน</th>
          <th colspan="4" class="b-orange">2. อนุมัต ผลตอบแทน</th>
          <th colspan="2" class="b-yellow">3. เตรียมเอกสาร</th>
          <th colspan="2" class="b-lime">4. ตรวจรับสัญญา</th>
          <th colspan="4" class="b-green">5. ส่งสัญญาตั้งเบิก</th>
        </tr>
        <tr class="center aligned">
          <th>ครบอายุ</th>
          <th>จำนวน<br>สัญญา</th>
          <th>ต่อสัญญา<br>เสร็จ</th>
          <th>%เสร็จ</th>
          <th>ไม่ต่อ<br>สัญญา</th>
          <th>รอ<br>ดำเนินการ</th>
          <th>%รอ</th>

          <th>รอ<br>รับงาน</th>
          <th>ติดต่อ<br>เจรจา</th>
          <th>ยังไม่เมล<br>ขออนุมัต</th>
          <th>ส่งเมล<br>แล้ว</th>

          <th>อนุมัต<br>ต่อสัญญา</th>
          <th>ไม่ต่อ<br>สัญญา</th>
          <th>เจรจรา<br>เพิ่ม</th>
          <th>คงค้าง<br>อนุมัติ</th>

          <th>จัดทำ<br>เอกสาร</th>
          <th>ส่ง<br>เอกสาร</th>

          <th>ส่งคืน<br>เอกสาร</th>
          <th>รับ<br>เอกสาร</th>

          <th>ค่าเช่า</th>
          <th>ไม่มี<br>ค่าเช่า</th>
          <th>ค่าไฟ</th>
          <th>ไม่มี<br>ค่าไฟ</th>
        </tr>
      </thead>
      <tbody>


      </tbody>
    </table>

    </div>

    </body>
        
	<?php include 'config/footer.php' ?>
  <script>
    function callTable(selectedYear){

        $('#table_report3').dataTable({
          "select": true,
          "columnDefs": [
            {  className: "b-red",      "targets": [8,9,10] },
            {  className: "b-orange",   "targets": [12,13,14] },
            {  className: "b-yellow",   "targets": [15] },
            {  className: "b-lime",     "targets": [17] },
            {  className: "b-green",    "targets": [19,20,21,22] },
            {  className: "b-indigo",   "targets": [7,11,16,18] }
          ],
          "bDestroy": true,
          "bSort" : false,
          "bInfo": false,
          "paging": false,
          "searching": false,
          "ajax": {
              "url": 'function/r3.php?year='+selectedYear
          }
        });

    }
    $(document).ready(function () {

      var cur_date  = new Date();
      var cur_year  = cur_date.getFullYear();
      var start_year = 2016;

      display = '<option value="">เลือกปี</option>';
      for (year = cur_year+5; year >= start_year; year--) {
        display += '<option value="'+year+'">'+year+'</option>';
      }

      $('#year').html(display);
      $('#year').dropdown({
        'forceSelection': false
        });
      $('#year').dropdown('set selected', cur_year);
      callTable(cur_year);

      $('#btn_report3').click(function() {
        var selectedYear  = $('#year').val();
        callTable(selectedYear);
      });
    }); //jq DocReady
  </script>

</html>
