<!DOCTYPE html>
<?php 
    $page = -1; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
?>
	
<html lang="en">
	<head>
		<title> Report 4 </title>
		<?php include 'config/header.php' ?>
	</head>
   <style>
      tr,td,th {
        padding-left:   7px    !important;
        padding-right:  7px    !important;
      }
    </style> 
    <body>
        
    <?php include 'navbar.php' ?>
		<div style="padding: 14px; padding-top: 0px">	
    <div class="ui segments">	
      <div class="ui secondary segment">
        <h3>
          Report 4 : โครงการที่อนุมัตต่อสัญญา
        </h3>
      </div>
      <div class="ui segment">
        
        <div class="ui centered grid">
          <div class="eight wide column">
            <b>เดือนที่อนุมัติ</b>
            &nbsp;&nbsp;&nbsp;
            <select class="ui search selection dropdown" name="year" id="year">
              <option value="">เลือกปี</option>
            </select>
            &nbsp;
            <select class="ui dropdown" name="month" id="month">
              <option value="">เลือกเดือน</option>
            </select>
            &nbsp;&nbsp;&nbsp;
            <button class="ui right labeled icon button"  id="btn_report4">
              <i class="right arrow icon"></i>
              ตกลง
            </button>
          </div>  
        </div>
        
      </div>
    </div>

    <table class="fixed cell-border row-border hover order-column nowrap"
           cellspacing="0" width="100%" id="table_report4">
      <thead>
        <tr class="center aligned">
          <th rowspan="2"></th>
          <th rowspan="2">เลขที่<br>สัญญาใหม่</th>
          <th rowspan="2">ชื่อโครงการ</th>
          <th rowspan="2">ประเภท</th>
          <th rowspan="2">วันที่<br>ต่อสัญญา</th>
          <th rowspan="2">วันที่<br>ครบสัญญา</th>
          <th colspan="3">ผลตอบแทนใหม่</th>
          <th rowspan="2">ทีมต่อสัญญา</th>

          <th colspan="3">ข้อมูลเก่า</th>
          <th rowspan="2">วันที่<br>หมดสัญญา</th>
          <th colspan="3">ผลตอบแทนเดิม</th>

          <th rowspan="2">วันที่<br>อนุมัต</th>
          <th rowspan="2">วันที่<br>รับสัญญา</th>
          <th colspan="3">วันที่ส่งตั้งเบิก</th>
        </tr>
        <tr class="center aligned">
          <th>ค่าเช่า</th>
          <th>ค่าไฟ</th>
          <th>อื่นๆ</th>
          <th>เลขที่สัญญาเดิม</th>
          <th>ประเภทโครงการ</th>
          <th>ประเภท</th>
          <th>ค่าเช่า</th>
          <th>ค่าไฟ</th>
          <th>อื่นๆ</th>
          <th>ค่าเช่า</th>
          <th>ค่าเช่า</th>
          <th>ค่าเช่า</th>
        </tr>
      </thead>
      <tbody>

      </tbody>
    </table>

    </div>

    </body>
        
	<?php include 'config/footer.php' ?>
  <script>

    function callTable(selectedMonth, selectedYear){

      $('#table_report4').dataTable({
        "select": true,
        "scrollX": true,
        "fixedColumns": {
            "leftColumns": 9
        },
        "columnDefs": [{ 
          className: "dt-body-center", "targets": [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21]
        }],
        "bDestroy": true,
        "bSort" : false,
        "bInfo": false,
        //"paging": false,
        "searching": false,
        "ajax": {
            "url": 'function/r4.php?year='+selectedYear+'&month='+selectedMonth
        }
      });

      $('div.dataTables_length select').addClass('ui compact dropdown');
      $('div.dataTables_length select').dropdown();

    }

    $(document).ready(function () {

      var cur_date  = new Date();
      var cur_month = cur_date.getMonth() + 1;  //เรียกจากฟังก์ชั่นนี้ มกรา = 0
      var cur_year  = cur_date.getFullYear();
      var start_year = 2016;

      display = '<option value="">เลือกปี</option>';
      for (var year = cur_year; year >= start_year; year--) {
        display += '<option value="'+year+'">'+year+'</option>';
      }

      $('#year').html(display);
      $('#year').dropdown('set selected', cur_year);

      // ใส่เดือนลงในdropdown
      $.post('function/getstring.php?get=string&column=month', 
        function(out) {	
          var display = '<option value="">เลือกเดือน</option>';
            for (var i = 0; i < 12; i++) {
              if(out[i].month == null)
                break;
              display += '<option value="'+out[i].id+'">'+out[i].month+'</option>';
            }
          $('#month').html(display);
          $('#month').dropdown('set selected', cur_month);
          
        },'json'
      );

      $('#btn_report4').click(function() {
        var selectedYear  = $('#year').val();
        var selectedMonth = $('#month').val();
        
        callTable(selectedMonth, selectedYear);
      });

      //เข้ามาครั้งแรก เลือกเดือนปี ปัจจุบัน
      callTable(cur_month, cur_year);
      

    }); //jq DocReady
  </script>

</html>
