<!DOCTYPE html>
<?php 
    $page = -1; 
    if(!isset($_SESSION)) { 
      session_start(); 
    }
?>
	
<html lang="en">
	<head>
		<title> Report 6 </title>
		<?php include 'config/header.php' ?>
  </head>
  <style>
      tr,td,th {
        padding-left:   7px    !important;
        padding-right:  7px    !important;
      }
  </style> 

    <body>
        
    <?php include 'navbar.php' ?>
		<div style="padding: 14px; padding-top: 0px">	
    <div class="ui secondary segment">
      <h3>
        Report 6 : งานต่อสัญญาคงค้าง
        </h3>
    </div>

      <table class="fixed cell-border row-border hover order-column nowrap"
             cellspacing="0" width="100%" id="table_report6">
        <thead>
          <tr class="center aligned">
            <th rowspan="2"></th>
            <th rowspan="2">เลขที่<br>สัญญาใหม่</th>
            <th rowspan="2">ชื่อโครงการ</th>
            <th rowspan="2">Location<br>Code</th>
            <th rowspan="2">ประเภท</th>
            <th rowspan="2">วันที่<br>ต่อสัญญา</th>
            <th rowspan="2">วันที่<br>ครบสัญญา</th>
            <th colspan="3">ผลตอบแทนใหม่</th>
            <th rowspan="2">ทีมต่อสัญญา</th>

            <th colspan="3">ข้อมูลเก่า</th>
            <th rowspan="2">วันที่<br>หมดสัญญา</th>
            <th colspan="3">ผลตอบแทนเดิม</th>

            <th rowspan="2">วันที่<br>รอติดต่อ</th>
            <th rowspan="2">เหตุผล</th>
            <th rowspan="2"></th>
          </tr>
          <tr class="center aligned">
            <th>ค่าเช่า</th>
            <th>ค่าไฟ</th>
            <th>อื่นๆ</th>
            <th>เลขที่สัญญาเดิม</th>
            <th>ประเภทโครงการ</th>
            <th>ประเภท</th>
            <th>ค่าเช่า</th>
            <th>ค่าไฟ</th>
            <th>อื่นๆ</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>

    </div>
    </body>
        
	<?php include 'config/footer.php' ?>
  <script>

    function callTable(){

        $('#table_report6').dataTable({
          "select": true,
          "scrollX": true,
          "fixedColumns": {
              "leftColumns": 10
          },
          "columnDefs": [{ 
            className: "dt-body-center", "targets": [0,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
          }],
          "bDestroy": true,
          "bSort" : false,
          "bInfo": false,
          "paging": false,
          "searching": false,
          "ajax": {
              "url": 'function/r6.php'
          }
        });
    }

    $(document).ready(function () {
      
      callTable();

    });
  </script>
</html>
