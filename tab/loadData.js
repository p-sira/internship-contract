function loadTab(step){
  var menu_array = ['','เจรจา', 'อนุมัติ', 'เอกสารสัญญา', 'ตรวจรับสัญญา','ส่งสัญญาตั้งเบิก'];
  var flow_step = parseInt(step);
  var abs_flowstep = Math.abs(flow_step);
  for (var i = 1; i < menu_array.length; i++) {
    if(i < abs_flowstep) {				//ขั้นที่เสร็จแล้วให้มีเครื่องหมายถูก
        $('#tab'+i).html(menu_array[i] + '<i class="checkmark icon"></i>');
    }
    else if(i==abs_flowstep) {		//ขั้นปัจจุบันมีเครื่องหมายหมุนๆ
        if(flow_step < 0)
          $('#tab'+i).html('<b>'+menu_array[i] + '(ทำใหม่)</b><i class="blue asterisk loading icon"></i>');
        else
          $('#tab'+i).html('<b>'+menu_array[i] + '</b><i class="blue asterisk loading icon"></i>');
    }
    else {												//ขั้นที่ยังไม่ถึง เป็นสีเทา
        $('#tab'+i).addClass("disabled");
    }
  }
}

function loadDropdown(string, oldlog) {

    // 
    var doc_type = [];
    var doc_require = [];
    for (var i = 1; i <= string.length; i++) {
      if(string[i].doc_type == null)
        break;
      doc_require[i] = (string[i].doc_require).split(',');
      doc_type[i] = string[i].doc_type;
    }

                                    //โหลดค่า string มาใส่ dropdown 
    var display = '<option value="">เลือกประเภทสัญญา</option>';             //เอกสารที่ต้องนำส่ง flow3
    for (var i = 1; i < string.length; i++) {
        if(string[i].doc_type == null || string[i].doc_type == '')
        break;
        display += '<option value="'+string[i].id+'">'+string[i].doc_type+'</option>';
    }
    $('#select_document_type').html(display);

    var display = '<option value="">เลือกประเภทสัญญา</option>';             // form ของ flow1
    for (var i = 1; i < string.length; i++) {
        if(string[i].contract_type == null || string[i].contract_type == '')
        break;
        display += '<option value="'+string[i].id+'">'+string[i].contract_type+'</option>';
    }
    $('#contract_type').html(display);
    $('#o_contract_type').html(display);

    var display = '<option value="">เลือกสถานะสัญญา</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].contract_status == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].contract_status+'</option>';
    }
    $('#contract_status').html(display);
    $('#o_contract_status').html(display);

    var display = '<option value="">เลือกส่วนแบ่งรายได้</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].contract_pay_cost == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].contract_pay_cost+'</option>';
    }
    $('#contract_pay_cost').html(display);
    $('#o_contract_pay_cost').html(display);

    var display = '<option value="">เลือกประเภทค่าไฟ</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].elec_pay_type == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].elec_pay_type+'</option>';
    }
    $('#elec_pay_type').html(display);
    $('#o_elec_pay_type').html(display);

    var display = '';                                                       //หน่วยละ, เดือนละ
    for (var i = 1; i < string.length; i++) {
        if(string[i].elec_pay_rate == null)
        break;
        display += '<div class="item" data-value="'+string[i].id+'">'+string[i].elec_pay_rate+'</div>';
    }
    $('#elec_pay_rate .menu').html(display);

    var display = '';                                                       //เลือกค่าปรับ
    for (var i = 1; i < string.length; i++) {
        if(string[i].elec_fee == null)
        break;
        display += '<div class="item" data-value="'+string[i].id+'">'+string[i].elec_fee+'</div>';
    }
    $('#elec_fee .menu').html(display);

    var display = '<option value="">เลือกเดือน</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].month == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].month+'</option>';
    }
    $('#bank_pay_month').html(display);
    $('#o_bank_pay_month').html(display);

    var display = '<option value="">เลือกประเภทโหนด</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].node_type == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].node_type+'</option>';
    }
    $('#node_type').html(display);
    $('#o_node_type').html(display);

    var display = '<option value="">เลือกประเภทสัญญา</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].node_type2 == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].node_type2+'</option>';
    }
    $('#node_type2').html(display);
    $('#o_node_type2').html(display);

    var display = '<option value="">เลือกระดับโหนด</option>';
    for (var i = 1; i < string.length; i++) {
        if(string[i].node_step == null)
        break;
        display += '<option value="'+string[i].id+'">'+string[i].node_step+'</option>';
    }
    $('#node_step').html(display);
    $('#o_node_step').html(display);

                                        //โชว์ตัวเลือกเวลาที่บันทึก ของสัญญาเก่า
    var display = '<option value="">เลือกวันและเวลา</option>';
    for (var i = 0; i < oldlog.length; i++) {
        display += '<option value="'+oldlog[i].option_value+'">'+oldlog[i].option_text+'</option>';
    }
    $('#select_oldver').html(display);

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////    ONCHANGE     //////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('#elec_fee').dropdown({
    onChange: function(value, text, $selectedItem) {
      if(value == 1) {                                        //ไม่มีค่าปรับ
        $('[name=elec_fee2]').val('0');
      }
    }
  });

  $('#elec_pay_type').dropdown({
    onChange: function(value, text, $selectedItem) {
      if(value==1){                                           //ถ้าเลือกสำรองจ่ายไตรมาส
        $('#elec_pay_rate').dropdown('set selected', 2);      //ให้เป็นเดือนละ
      }
      else if(value == 5){                                     //ถ้าไม่มีค่าไฟฟ้า
        $('#elec_pay_rate').dropdown('set selected', 1);
        $('[name=elec_pay_rate2]').val(0);
        $('#elec_fee').dropdown('set selected', 1);
        $('[name=elec_fee2]').val('0');
      }
      else{
        $('#elec_pay_rate').dropdown('set selected', 1);      //อื่นๆ ให้เป็นหน่วยละ
      }
    }
  });

    // dropdown ทุกครั้งที่ กด จะเปลี่ยนข้อมูลโชว์ด้านล่าง
  $('#select_document_type').dropdown({		  
    onChange: function() {
      var doctype = $(this).val();
      var display = '<label><b>เอกสารที่ต้องใช้</b></label><ul class="ui list">';
      for (var i = 0; i < doc_require[doctype].length; i++) {
        //doctype-1 เพราะ อาเรมันเริ่มจาก 0 แต่ doctype มันเริ่มจาก 1-5
        if(doc_require[doctype][i] == null)
          break;
        display += '<li>'+ doc_require[doctype][i] +'</li>';
      }
      display+= '</ul><br>';
      $('#show_document').html(display);
    }
  });
}