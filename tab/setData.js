function setData(data, s){

    var f         = data.f;
    var step      = data.m['flow_step'];
    var abs_step  = Math.abs(step);

    var temp1;
    var temp2;
    var cur_date  = moment();
    var end_date  = moment(data.co['contract_end']);
    var end       = end_date.format('DD MMM YYYY');
    var date_diff = end_date.fromNow();

    var doc_type = [];
    var doc_require = [];
    for (var i = 1; i <= s.length; i++) {
      if(s[i].doc_type == null)
        break;
      doc_require[i] = (s[i].doc_require).split(',');
      doc_type[i] = s[i].doc_type;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////

    if(abs_step > 1) {                                                    //ผ่านเจรจาแล้ว
      temp1 = moment(f['flow1_finish']).format('DD MMM YYYY');
      $("#dimmer_success_newcontract").dimmer('show');
      $("#dimmer_success_newcontract_subheader").html('วันที่ '+temp1);
      if(data.f['flow2_sendmail'] == '1' && data.cn['mustsendmail'] == '1'){  //ถ้าส่งเมลแล้ว
        $("#dimmer_mailsend").dimmer('show');
      }
      else if(data.f['flow2_sendmail']!='1' && data.cn['mustsendmail']=='1'){ //ยังไม่เมล                                       //ถ้ายังไม่ส่งเมล
        $("#detail2_info2").html('\
          <div class="ui warning message">\
            <div class="header">ยังไม่ส่งอีเมลจากระบบ</div>\
            <p>ส่งอีเมลที่กล่องข้อความด้านล่าง</p>\
          </div>\
        ');
      }
    }
    if(abs_step > 2){                                                     //ผ่านอนุมัติแล้ว
        temp1 = moment(f['flow2_finish']).format('DD MMM YYYY');
        $("#dimmer_success_mail").dimmer('show');
        $("#dimmer_success_approve").dimmer('show');
        $("#dimmer_success_mail_subheader").html('วันที่ '+temp1);
        $("#dimmer_success_approve_subheader").html('วันที่ '+temp1);
    }
    if(abs_step > 3){                                                     //ผ่านส่งสัญญาแล้ว
        temp1 = moment(f['flow3_finish']).format('DD MMM YYYY');
        $("#dimmer_success_selectdoc").dimmer('show');
        $("#dimmer_success_selectdoc_subheader").html('วันที่ '+temp1+'<br>(เอกสารประเภท'+doc_type[data.d['doc_type']]+')');
    }
    if(abs_step > 4){                                                     //รับสัญญาแล้ว
        temp1 = moment(f['flow4_finish']).format('DD MMM YYYY');
        $("#dimmer_success_savedoc").dimmer('show');
        $("#dimmer_success_savedoc_subheader").html('วันที่ '+temp1+'<br>(เอกสารประเภท'+doc_type[data.d['doc_type']]+')');
    }

  // disable ปุ่ม/ซ่อน ถ้ายังไม่ถึงขั้น ->
    if(step==0) { 											                    // ถ้ายังไม่ได้กดรับงาน ห้ามบันทึกสัญญา
      $('#btn_save_contract').prop('disabled', true);
    }
    if(abs_step != 1) {                                     // ถ้าไม่ใช่ขั้น เจรจา
      $('#btn_save_contract').prop('disabled', true);
      $("#dimmer_disable_newcontract").dimmer('show');
      $("#dimmer_disable_oldcontract").dimmer('show');
    }
    if(abs_step != 2) {                                     // ถ้าไม่ใช่ขั้น อนุมัติ -> disable ปุ่มหน้าอนุมัติ
      $('#btn_notapprove').prop('disabled', true);
      $('#btn_talkagain').prop('disabled', true);
      $('#btn_approve').prop('disabled', true);
      $('#btn_sendmail').prop('disabled', true);
      $("#dimmer_disable_mail").dimmer('show');
      $("#dimmer_disable_approve").dimmer('show');
    }
    if(abs_step != 3) {                                     // ถ้าไม่ใช่ขั้น เอกสารสัญญา -> disable ปุ่ม
      $('#btn_selectdoc').prop('disabled', true);
      $("#dimmer_disable_selectdoc").dimmer('show');
    }
    if(abs_step != 4) { 											              //ถ้าไม่ใช่ขั้น ตรวจสัญญา -> disable ปุ่ม
      $('#btn_savedoc').prop('disabled', true);
      $('#btn_rejectdoc').prop('disabled', true);
      $("#dimmer_disable_savedoc").dimmer('show');
    }
    if(abs_step != 5) { 										                // ถ้าไม่ใช่ขั้น ส่งสัญญาตั้งเบิก
      $('#btn_endwork').prop('disabled', true);
      $("#dimmer_disable_endwork").dimmer('show');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    
    $('#head_project_name').html('โครงการ'+data.p['project_name']);

    if(data.p['lot_status'] == 1) {
      $('#project_status').html('<ul><li>Node นี้ยกเลิกแล้ว</li></ul>');
      $('#message_project_status').addClass('negative');
    }
    else {
      if(end_date < cur_date){
        var status = 'สัญญาหมดเมื่อ';
        $('#message_project_status').addClass('negative');
      }
      else{
        var status = 'เหลือเวลา';
        $('#message_project_status').addClass('positive');
      }

      $('#project_status').html('\
        <ul><li>สัญญาหมดในวันที่ '+ end +'</li>\
            <li>'+status+ date_diff +'</li>\
        </ul>\
      ');
    }

    $('#p_name').html(data.p['project_name']);
    $('#p_no').html(data.p['project_number']);
    $('#p_code').html(data.p['project_location_code']);
    $('#p_per_name').html(data.p['lot_person']);
    $('#p_per_tel').html(data.p['lot_tel']);
    $('#p_address').html(data.p['lot_address']);
    $('#p_district').html(data.p['lot_district']);

    temp1 = parseInt(data.p['lot_type']);
    temp2 = isNaN(temp1) ? data.p['lot_type'] : s[temp1].lot_type;
    $('#p_lot_type').html(temp2);
    
    temp1 = parseInt(data.p['lot_status']);
    temp2 = isNaN(temp1) ? data.p['lot_status'] : s[temp1].lot_status;
    $('#p_lot_status').html(temp2);

    $('#m_number').html(data.me['meter_number']);
    $('#m_c_number').html(data.me['meter_contract_number']);
    $('#m_RP').html(data.me['meter_RP']);
    $('#m_plan').html(data.me['meter_plan']);
    $('#m_status').html(data.me['meter_status']);
    $('#m_doc1').html(data.me['meter_doc1']);
    $('#m_doc2').html(data.me['meter_doc2']);
    $('#m_send').html(data.me['meter_send']);
    $('#m_check').html(data.me['meter_check']);
    $('#m_start').html(data.me['meter_start_date']);
    $('#m_something_number').html(data.me['meter_something_number']);
    $('#m_remark').html(data.me['meter_remark']);
    $('#m_who_pay').html(data.me['meter_who_pay']);

    $("[name=contract_number]").val(data.cn['contract_number']);
    $("[name=contract_person_name]").val(data.cn['contract_person_name']);
    $("[name=contract_type]").val(data.cn['contract_type']);
    $("[name=contract_start]").val(data.cn['contract_start']);
    $("[name=contract_end]").val(data.cn['contract_end']);
    $("[name=contract_year]").val(data.cn['contract_year']);
    $('#contract_type').dropdown('set selected', data.cn['contract_type']);
    $('#contract_status').dropdown('set selected', data.cn['contract_status']);
    $("[name=contract_1st_cost]").val(data.cn['contract_1st_cost']);
    $("[name=contract_annual_cost]").val(data.cn['contract_annual_cost']);
    $('#contract_pay_cost').dropdown('set selected', data.cn['contract_pay_cost']);
    
    $('#elec_pay_type').dropdown('set selected', data.cn['elec_pay_type']);

    if(data.cn['elec_pay_type'] == '1') {                                 //ถ้าสำรองจ่ายไตรมาส -> ให้เรตเป็นเดือนละ
      $('#elec_pay_rate').dropdown('set selected', '2');
    }
    else{
      $('#elec_pay_rate').dropdown('set selected', data.cn['elec_pay_rate']);
    }
    
    //$("[name=elec_pay_rate2]").val(data.cn['elec_pay_rate2']);

    $('#elec_fee').dropdown('set selected', data.cn['elec_fee']);
    $("[name=elec_fee2]").val(data.cn['elec_fee2']);
    
    $("[name=contract_other_benefit]").val(data.cn['contract_other_benefit']);

    $("[name=bank_owner_name]").val(data.cn['bank_owner_name']);
    $("[name=bank_code_name]").val(data.cn['bank_code_name']);
    $("[name=bank_number]").val(data.cn['bank_number']);
    $('#bank_pay_month').dropdown('set selected', data.cn['bank_pay_month']);
    $("[name=bank_owner_contact1]").val(data.cn['bank_owner_contact1']);
    $("[name=bank_owner_contact2]").val(data.cn['bank_owner_contact2']);

    $('#node_type').dropdown('set selected', data.cn['node_type']);
    $('#node_type2').dropdown('set selected', data.cn['node_type2']);
    $('#node_step').dropdown('set selected', data.cn['node_step']);

    $('#checksendmail').prop('checked', true);              // default = ต้องส่งเมล

    if(step == -1){
        display += '<div class="ui error message">\
                    <div class="header"> เจรจาใหม่ </div>\
                    <ul class="list">\
                    <li>เหตุผล: '+data.d['mail_remark']+'</li></ul></div>';
        $('#detail1_info2').html(display);
    }

    else if(step == -3){
      var type = data.d['doc_type'];
      var have = data.d['doc'].split(',');
      var have2 = [0,0,0,0,0,0,0,0];
      var display = '';
      var count = 0;
      for (var i = 0; i < doc_require[type].length; i++) {
        have2[have[i]] = 1;
      }
      
      display += '<div class="ui error message">\
                    <div class="header"> เอกสารถูกตีกลับ </div>\
                    <ul class="list">\
                    <li>เหตุผล: '+data.d['doc_remark']+'</li>';
     
      display += '<li>ขาดเอกสาร: ';
      for (var i = 0; i < doc_require[type].length; i++) {
        if(have2[i] == 0){
          display += '('+doc_require[type][i]+'), ';
        }
        else{
          count++;
        }
      }
      if(count == doc_require[type].length)
        display += ' '+เอกสารครบ;
      display += '</li>';
      display += '</ul></div>';
      $('#detail3_info2').html(display);
    }

    else if(step >= 4) {	                             //อยู่ขั้นตรวจเอกสาร หรือ ผ่านขั้นตรวจเอกสารแล้ว
      if(data.d['doc_type'] == null) {
          alert('ประเภทเอกสารในแท็บ "ตรวจเอกสาร" ผิดพลาด');
      }	else {

        $('#select_document_type').dropdown('set selected',data.cn['doc_type']);
        var type = data.d['doc_type']; 
        var display = '<label><b>เอกสารที่ต้องใช้</b></label><ul class="ui list">';
          for (var i = 0; i < doc_require[type].length; i++) {
            if(doc_require[type][i] == null)
              break;
            display += '<li>'+ doc_require[type][i] +'</li>';
          }
          display+= '</ul><br>';

        // โชว์ข้อมูลเอกสารที่ต้องใช้ในแท็บ 3  
        $('#show_document').html(display);                                      

        display = ' <div><h3>เอกสารสัญญาสำหรับ '+ doc_type[type] +'</h3></div><br>\
                    <div class="ui form">\
                      <div class="grouped fields">\
                      <label>เช็คเอกสาร</label>';
          for (var i = 0; i < doc_require[type].length; i++) {
            display += '<div class="field">\
                        <div class="ui checkbox">\
                          <input type="checkbox" name="check_1" value="'+i+'">\
                          <label>'+ doc_require[type][i] +'</label>\
                        </div>\
                        </div>';
          }
          display += '</div> \
                        <div class="field"><label>รายละเอียดเพิ่มเติม</label><textarea id="doc_remark"></textarea></div>\
                      </div>';

        // โชว์ข้อมูลเอกสารที่ต้องใช้ในแท็บ 4
        $('#show_check_document').html(display);
      }
    }
    

    if(data.d['mail_file_path']){
      $('#show_mail_image').html('\
        <h4>สำเนาอีเมล <i class="file image outline icon"></i></h4>\
        <a class="ui button" href="'+data.d['mail_file_path']+'" download="สำเนาอีเมล">\
        <i class="download icon"></i>ดาวน์โหลด</a>\
      ');
    }
    if(data.d['doc_file_path']) {
      $('#show_doc_copy').html('\
        <h4>สำเนาสัญญา <i class="file pdf outline icon"></i></h4>\
        <a class="ui button" href="'+data.d['doc_file_path']+'" download="สำเนาสัญญา.pdf">\
        <i class="download icon"></i>ดาวน์โหลด</a>\
      ');
    }


}