function setTab1(project_id){
  
  $('#form_new_contract').form({
    inline: true,
    fields: {		// validate ->
      contract_number: {
        identifier: 'contract_number',
        rules: [{
            type   : 'empty',       prompt : 'โปรดกรอกเลขที่สัญญา'
        },{
            type   : 'number',       prompt : 'เลขเท่านั้น'
        }]
      },
      contract_type: {
        identifier: 'contract_type',
        rules: [{
            type   : 'empty',       prompt : 'โปรดเลือกประเภทสัญญา'
        }]
      },
      contract_start: {
        identifier: 'contract_start',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่วันเริ่มสัญญา'
        }]
      },
      contract_end: {
        identifier: 'contract_end',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่วันสิ้นสุดสัญญา<br> ถ้าไม่กำหนด ให้ใส่วันเดียวกับวันเริ่มสัญญา'
        }]
      },
      contract_year: {
        identifier: 'contract_year',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่จำนวนปี<br> ถ้าไม่กำหนด ให้ใส่ \'0\''
        },{
            type   : 'number',       prompt : 'เลขเท่านั้น'
        }]
      },
      contract_1st_cost: {
        identifier: 'contract_1st_cost',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่ค่าบำรุงแรกเริ่ม<br> ถ้าไม่มี ให้ใส่ \'0\''
        },{
            type   : 'number',       prompt : 'เลขเท่านั้น'
        }]
      },
      contract_annual_cost: {
        identifier: 'contract_annual_cost',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่ค่าเช่าต่อปี<br> ถ้าไม่มี ให้ใส่ \'0\''
        },{
            type   : 'number',       prompt : 'เลขเท่านั้น'
        }]
      },
      contract_pay_cost: {
        identifier: 'contract_pay_cost',
        rules: [{
            type   : 'empty',       prompt : 'โปรดเลือกส่วนแบ่งรายได้'
        }]
      },
      elec_pay_type: {
        identifier: 'elec_pay_type',
        rules: [{
            type   : 'empty',       prompt : 'โปรดเลือกค่าไฟ'
        }]
      },
      elec_pay_rate2: {
        identifier: 'elec_pay_rate2',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่อัตราค่าไฟ ถ้าไม่มี ให้ใส่ \'0\''
        },{
            type   : 'number',       prompt : 'เลขเท่านั้น'
        }]
      },
      elec_fee2: {
        identifier: 'elec_fee2',
        rules: [{
            type   : 'empty',       prompt : 'โปรดใส่อัตราค่าปรับ ถ้าไม่มี ให้ใส่ \'0\''
        },{
            type   : 'number',       prompt : 'เลขเท่านั้น'
        }]
      },
      node_type: {
        identifier: 'node_type',
        rules: [{
            type   : 'empty',       prompt : 'โปรดเลือกประเภทโหนด'
        }]
      },
      node_type2: {
        identifier: 'node_type2',
        rules: [{
            type   : 'empty',       prompt : 'โปรดเลือกประเภทสัญญา'
        }]
      },
      node_step: {
        identifier: 'node_step',
        rules: [{
            type   : 'empty',       prompt : 'หากไม่ใช่สัญญาร่วมให้เลือก \'โหนดหลัก\''
        }]
      },
    },
    onSuccess: function(event, fields) {
      event.preventDefault();

      if(confirm('บันทึกข้อมูล?')){
        var data = $('#form_new_contract').serializeArray();
        var mustsendmail = $('#checksendmail').is(':checked') ? 1:0;  //check = 1, not check = 0
        data.push({name: 'project_id', value: project_id});

        //ดรอปดาวข้างอินพุตไม่ยอมติดมาด้วย ต้องยัดเอง
        data.push({name: 'elec_pay_rate', value: $('#elec_pay_rate').dropdown('get value')});
        data.push({name: 'elec_fee', value: $('#elec_fee').dropdown('get value')});

        //ค่า checkbox
        data.push({name: 'mustsendmail', value: mustsendmail});

        console.log(data);
        $.post('function/f1_savecontract.php', data, 
          function(output) {
            location.href = 'p_information.php?id='+project_id+'#tab2';
            location.reload();
          },'json'
        ).fail(function(xhr, status, error) {
            console.log(xhr.responseText);
          });
      }
      return false;
    }
  });

  $('#select_oldver').dropdown({
    forceSelection: false,
    onChange: function() {
      var log_id = $(this).val();
      $.post('function/f1_oldcontract.php', {log_id: log_id}, 
        function (o) {
          if(o.bool) {
            $("[name=o_contract_number]").val(o.old['contract_number']);
            $("[name=o_contract_person_name]").val(o.old['contract_person_name']);
            $("[name=o_contract_type]").val(o.old['contract_type']);
            $("[name=o_contract_start]").val(o.old['contract_start']);
            $("[name=o_contract_end]").val(o.old['contract_end']);
            $("[name=o_contract_year]").val(o.old['contract_year']);
            $("[name=o_contract_type]").val(o.old['contract_type']);
            $("[name=o_contract_status]").val(o.old['contract_status']);
            $("[name=o_contract_1st_cost]").val(o.old['contract_1st_cost']);
            $("[name=o_contract_annual_cost]").val(o.old['contract_annual_cost']);
            $("[name=o_contract_pay_cost]").val(o.old['contract_pay_cost']);
            $("[name=o_elec_pay_type]").val(o.old['elec_pay_type']);
            $("[name=o_elec_pay_rate]").val(o.old['elec_pay_rate']);
            $("[name=o_elec_fee]").val(o.old['elec_fee']);
            $("[name=o_contract_other_benefit]").val(o.old['contract_other_benefit']);

            $("[name=o_bank_owner_name]").val(o.old['bank_owner_name']);
            $("[name=o_bank_code_name]").val(o.old['bank_code_name']);
            $("[name=o_bank_number]").val(o.old['bank_number']);
            $("[name=o_bank_pay_month]").val(o.old['bank_pay_month']);
            $("[name=o_bank_owner_contact1]").val(o.old['bank_owner_contact1']);
            $("[name=o_bank_owner_contact2]").val(o.old['bank_owner_contact2']);

            $("[name=o_node_type]").val(o.old['node_type']);
            $("[name=o_node_type2]").val(o.old['node_type2']);
            $("[name=o_node_step]").val(o.old['node_step']);
            $("[name=o_node_child]").val(o.old['node_child']);
          }
        },'json'
      );
    }
  });
}