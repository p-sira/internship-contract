function setTab2(project_id, p, co, cn, string){

    var temp1;
    var temp2;

  //รายละเอียดในกล่องเมล
    $("[name=mail_subject]").val('[สัญญา] ต่อสัญญาโครงการ '+p['project_name']);

    $('.multiple.search.selection.dropdown').dropdown({
			allowAdditions: true
    });
      
    $('#mail_to').dropdown('set selected',['sittha.s@mail.com']);
    $('#mail_cc').dropdown('set selected',['fuseela.fl@gmail.com', 'darroct.blues@gmail.com']);

    co['contract_other_benefit'] = co['contract_other_benefit']==null? '' : co['contract_other_benefit'];

    ////////////////   เปลี่ยน INT เป็น STRING   /////////////////////////////////////////////////////////////
    temp1 = parseInt(co['contract_pay_cost']);
    co['contract_pay_cost'] = isNaN(temp1) ? co['contract_pay_cost'] : string[co['contract_pay_cost']].contract_pay_cost;
    temp1 = parseInt(cn['contract_pay_cost']);
    cn['contract_pay_cost'] = isNaN(temp1) ? cn['contract_pay_cost'] : string[cn['contract_pay_cost']].contract_pay_cost;

    temp1 = parseInt(co['elec_pay_type']);
    co['elec_pay_type'] = isNaN(temp1) ? co['elec_pay_type'] : string[co['elec_pay_type']].elec_pay_type;
    temp1 = parseInt(cn['elec_pay_type']);
    cn['elec_pay_type'] = isNaN(temp1) ? cn['elec_pay_type'] : string[cn['elec_pay_type']].elec_pay_type;

    temp1 = parseInt(co['elec_pay_rate']);
    co['elec_pay_rate'] = isNaN(temp1) ? co['elec_pay_rate'] : string[co['elec_pay_rate']].elec_pay_rate;
    temp1 = parseInt(cn['elec_pay_rate']);
    cn['elec_pay_rate'] = isNaN(temp1) ? cn['elec_pay_rate'] : string[cn['elec_pay_rate']].elec_pay_rate;

    temp1 = parseInt(co['elec_fee']);
    co['elec_fee'] = isNaN(temp1) ? co['elec_fee'] : string[co['elec_fee']].elec_fee;
    temp1 = parseInt(cn['elec_fee']);
    cn['elec_fee'] = isNaN(temp1) ? cn['elec_fee'] : string[cn['elec_fee']].elec_fee;

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    cn['contract_end']   = moment(cn['contract_end']).format('DD MMMM YYYY');;
    cn['contract_start'] = moment(cn['contract_start']).format('DD MMMM YYYY');;
    
    var display = '';
        display = 'เรียน คุณสิทธา'+'\n\n';
        display+= 'ขออนุมัติสิทธิประโยชน์ให้กับโครงการ '+p['project_name']+' ดังนี้'+'\n';
        display+= '\n- สัญญาวันที่ '+cn['contract_start']+' ถึง '+cn['contract_end']+
              ' เป็นระยะเวลา '+cn['contract_year']+' ปี';

    display+= '\n- ค่าบำรุงเริ่มต้น';
      if(cn['contract_1st_cost'] == co['contract_1st_cost'])
          display+=' ไม่เปลี่ยนแปลง ('+cn['contract_1st_cost']+' บาท)';
      else{
          display+= ' จาก '+co['contract_1st_cost']+' บาท เป็น '+cn['contract_1st_cost']+' บาท';
      }

    display+= '\n- อัตราค่าเช่าต่อปี';
      if(cn['contract_annual_cost'] == co['contract_annual_cost'])
          display+=' ไม่เปลี่ยนแปลง ('+cn['contract_annual_cost']+' บาท)';
      else{
          display+= ' จาก '+co['contract_annual_cost']+' บาท เป็น '+cn['contract_annual_cost']+' บาท';
      }
    
    display+= '\n- ส่วนแบ่งรายได้';
      if(cn['contract_pay_cost'] == co['contract_pay_cost'])
          display+=' ไม่เปลี่ยนแปลง ('+cn['contract_pay_cost']+')';
      else{
          display+= ' จาก '+co['contract_pay_cost']+
                    ' เป็น '+cn['contract_pay_cost'];
      }

    display+= '\n- ค่าไฟ';
      if(cn['elec_pay_type'] == co['elec_pay_type']
        && cn['elec_pay_rate'] == co['elec_pay_rate'] 
        && cn['elec_pay_rate2'] == co['elec_pay_rate2']
        && cn['elec_fee'] == co['elec_fee']
        && cn['elec_fee2'] == co['elec_fee2']
      ){

          display+= ' ไม่เปลี่ยนแปลง ('+cn['elec_pay_type']+
                    ' '+cn['elec_pay_rate']+
                    ' '+cn['elec_pay_rate2']+
                    ' ('+cn['elec_fee']+' '+cn['elec_fee2']+')';
      } 
      else{
          display+= ' จาก '+co['elec_pay_type']+
                    ' '+co['elec_pay_rate']+' '+co['elec_pay_rate2']+
                    ' ('+co['elec_fee']+
                    ') เป็น '+cn['elec_pay_type']+
                    ' '+cn['elec_pay_rate']+
                    ' '+cn['elec_pay_rate2']+
                    ' ('+cn['elec_fee']+
                    ' '+cn['elec_fee2']+')';
      }
    
      if(cn['contract_other_benefit'] == co['contract_other_benefit']){
        if(cn['contract_other_benefit'] != ''){
          display+= '\n- สิทธิประโยชน์อื่นๆ';
          display+=' ไม่เปลี่ยนแปลง ('+cn['contract_other_benefit']+')';
        }
      }
      else{
          display+= '\n- สิทธิประโยชน์อื่นๆ';
          display+= ' จาก '+co['contract_other_benefit']+' เป็น '+cn['contract_other_benefit'];
      }

    display+= '\n\nจึงเรียนมาเพื่อพิจารณา';

    $('#mail_detail').val(display); //แสดงผล

 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
  $('#btn_sendmail').click(function() {
    if(confirm('ส่งอีเมล?')) {
      var $loader_mail = $('#loader_mail');
      $loader_mail.dimmer('show');
      var data = $('#form_sendmail').serializeArray();
      data.push({name: 'project_id', value: project_id});
      data.push({name: 'mail_to', value: $('#mail_to').dropdown('get value') });
      data.push({name: 'mail_cc', value: $('#mail_cc').dropdown('get value') });
      $.post('function/f2_sendmail.php', data, 
        function(output) {
          if(!output.bool)
            alert('ผิดพลาด '+output.text);	//ผิดพลาด
          else {
            $loader_mail.dimmer('hide');
            $('#dimmer_mail_success').dimmer('show');
          
            location.href = 'p_information.php?id='+project_id+'#tab2';
            location.reload();
          }
        },'json'
      ).fail(function(xhr, status, error) {
        console.log(xhr.responseText);
      });
    }
  });

  // เช็คประเภทไฟล์ + ขนาดไฟล์ก่อนอัพ
  $('#file_email').on( 'change', function() {
    myfile = $(this).val();
    var ext = myfile.split('.').pop();
    if(ext=="jpeg" || ext=="jpg" || ext=="png" || ext=="pdf" || ext=="msg"){		
        if(this.files[0].size/1024/1024 > 2){
          alert('อัพโหลดไฟล์ได้ไม่เกิน 2 mb');
          $(this).val(''); 
        }
    } else{
        alert('อัพโหลดไฟล์ประเภท jpeg/jpg/png/pdf เท่านั้น');
        $(this).val(''); 
    }
  });

  // อัพไฟล์สำเนาอีเมล แล้วอนุมัติไปขั้นถัดไป
  $('#btn_approve').click(function(event){
    event.preventDefault();
    if(confirm('ยืนยันอนุมัติสัญญา ใช่หรือไม่? (หากสัญญาไม่เปลี่ยนแปลง ไม่จำเป็นต้องอัพโหลดสำเนา)')){
      var form_data = new FormData();     
            
      var approve_remark = $('#approve_remark').val();
      var file_data = $('#file_email').prop('files')[0];   
      if($('#file_email').get(0).files.length !== 0 ){
        form_data.append('file_email', file_data);
      }

      form_data.append('project_id', project_id);
      form_data.append('remark', approve_remark);


      for (var key of form_data.entries()) {
        console.log(key[0] + ', ' + key[1]);
      }

      //อัพไฟล์
      $.ajax({
        url: 'function/f2_approve.php',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(out){
          if(out.bool) { //อัพสำเร็จ รีหน้า
            location.href = 'p_information.php?id='+project_id+'#tab3';
            location.reload();
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.log(xhr.responseText);
        }
      });
    }
  });

  // เจรจาใหม่ (ย้อนไป flow 1)
  $('#btn_talkagain').click(function(event){
    event.preventDefault();
    if($('#approve_remark').val() == ''){
      alert('กรอกเหตุผลที่ต้องเจรจาใหม่');
    }
    else if(confirm('ส่งกลับไปเจรจาต่อรองใหม่ ใช่หรือไม่?')){
      var approve_remark = $('#approve_remark').val();
      $.post('function/f2_talkagain.php', {project_id: project_id, remark:approve_remark},
        function() {
          location.href = 'p_information.php?id='+project_id+'#tab1';
          location.reload();
        }
      ).fail(function(xhr, status, error) {
        console.log(xhr.responseText);
      });
    }
  });
    
  //ไม่อนุมัติ ยกเลิก node เลย
  $('#btn_notapprove').click(function(event){
    event.preventDefault();
    if($('#approve_remark').val() == ''){
      alert('กรอกเหตุผลที่ต้องปิด Node');
    }
    else if(confirm('ไม่อนุมัติสัญญา ใช่หรือไม่? (หากเลือกตกลง จะเป็นการยกเลิก Node แล้วปิดงานทันที)')){
      var form_data = new FormData();     
      var approve_remark = $('#approve_remark').val();
      var file_data = $('#file_email').prop('files')[0];   
        if(file_data != null)
          form_data.append('file_email', file_data);
        
        form_data.append('project_id', project_id);
        form_data.append('remark', approve_remark);

      // อัพไฟล์
      $.ajax({
        url: 'function/f2_notapprove.php',
        dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,                         
        type: 'post',
        success: function(out){
          if(out.bool) { //อัพสำเร็จ รีหน้า
            location.href = 'p_information.php?id='+project_id;
            location.reload();
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          console.log(xhr.responseText);
        }
      });
    }
  });

}