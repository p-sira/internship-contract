function setTab3(project_id){


  //ปุ่มยืนยันประเภทสัญญา
  $('#btn_selectdoc').on('click', function(event) {
    var doctype = $('#select_document_type').val();
    if(doctype == '')
      alert('เลือกประเภทเอกสารก่อน');
    else{
      if(confirm('พร้อมส่งเอกสารใช่หรือไม่?')){
        event.preventDefault();
        $.post('function/f3_selectdoc.php', {project_id : project_id, doc_type: doctype},
          function() {
              location.href = 'p_information.php?id='+project_id+'#tab4';
              location.reload();
          }
        ).fail(function(xhr, status, error) {
          console.log(xhr.responseText);
        });
      }
    }
  });	
}