function setTab4(project_id){
  // เช็คประเภทไฟล์ + ขนาดไฟล์ก่อนอัพ
  $('#file_doc').on( 'change', function() {
    myfile = $(this).val();
    var ext = myfile.split('.').pop();
    if(ext=="pdf"){		
      if(this.files[0].size/1024/1024 > 2){
        alert('อัพโหลดไฟล์ได้ไม่เกิน 2 mb');
        $(this).val(''); 
      }
    } else{
      alert('อัพโหลดไฟล์ประเภท pdf เท่านั้น');
      $(this).val(''); 
    }
  });

  // อัพเอกสาร + เช็คว่าติ้กเอกสารครบทุกฉบับมั้ย
  $('#btn_savedoc').click(function(event){
    event.preventDefault();
    var checkedValues = $('[name=check_1]:checked').map(function() {
        return this.value;
    }).get();
    
    var notcheck = $('[name=check_1]:not(:checked)').map(function() {
        return this.value;
    }).get();
    if(notcheck.length != 0)
      alert('เอกสารต้องครบ ถึงจะยืนยันรับเอกสารได้');
    else if($('#file_doc').val() == '') 
      alert('เลือกสำเนาสัญญาก่อน');
    else {
      if(confirm('ยืนยันรับเอกสารใช่หรือไม่')){
        var form_data = new FormData();
        var file_data = $('#file_doc').prop('files')[0];
        var doc_remark = $('#doc_remark').val();

        form_data.append('file_doc', file_data);
        form_data.append('project_id', project_id);
        form_data.append('doc_remark', doc_remark);
        form_data.append('documents', checkedValues);

      // อัพไฟล์
        $.ajax({
          url: 'function/f4_savedoc.php', 
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,                         
          type: 'post',
          success: function(output) {
            if(output.bool) { //อัพสำเร็จ bool = 1, รีหน้า
                location.href = 'p_information.php?id='+project_id+'#tab5';
                location.reload();
            }
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
          }
        });
      }
    }
  });

  // ปฏิเสธเอกสารสัญญา
  $("#btn_rejectdoc").on('click', function(event){
    event.preventDefault();
    var checkedValues = $('[name=check_1]:checked').map(function() {
        return this.value;
    }).get();
    var doc_remark = $('#doc_remark').val();
    if(confirm('ปฏิเสธเอกสารสัญญา?')) {
      if(doc_remark=='')
        alert('กรอกเหตุผลด้วย');
      else {
        var form_data = new FormData();

        form_data.append('project_id', project_id);
        form_data.append('doc_remark', doc_remark);
        form_data.append('documents', checkedValues);

        $.ajax({
          url: 'function/f4_rejectdoc.php', 
          dataType: 'json',
          cache: false,
          contentType: false,
          processData: false,
          data: form_data,                         
          type: 'post',
          success: function() {
              location.href = 'p_information.php?id='+project_id+'#tab3';
              location.reload();
          },
          error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.responseText);
          }
        });
      }
    }
  });
}