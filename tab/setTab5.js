function setTab5(project_id){
  $("#btn_endwork").on('click', function(event){
    event.preventDefault();
    if(confirm('ยืนยันปิดงาน')) {
      $.post('function/f5_endwork.php', {project_id : project_id},
        function (output) {   
          location.href = 'p_information.php?id='+project_id+'#tab5';
          location.reload();
        }
      ).fail(function(xhr, status, error) {
        console.log(xhr.responseText);
      });
    }
  });
}